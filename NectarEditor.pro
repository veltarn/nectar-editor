#-------------------------------------------------
#
# Project created by QtCreator 2013-07-16T18:10:08
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NectarEditor
TEMPLATE = app


SOURCES += main.cpp\
        nectareditormainwindow.cpp \
    Gui/optionswindow.cpp \
    Core/projectmanager.cpp \
    Core/project.cpp \
    Gui/projectpropertieswindow.cpp \
    Gui/newprojectwindow.cpp \
    Graphics/graphicalview.cpp \
    Graphics/graphicscene.cpp \
    Graphics/graphicsview.cpp \
    Graphics/abstracttool.cpp \
    Graphics/brushtool.cpp \
    Graphics/groundmaterial.cpp \
    Graphics/basicmaterial.cpp \
    Graphics/stonematerial.cpp \
    Graphics/erasertool.cpp \
    Graphics/sandmaterial.cpp \
    Graphics/woodmaterial.cpp \
    Graphics/watermaterial.cpp \
    Core/materialselector.cpp \
    Core/PakFileException.cpp \
    Core/Pak.cpp \
    Widgets/listwidget.cpp \
    Gui/firststartdialog.cpp \
    Widgets/gradientwidget.cpp \
    Core/conversion.cpp

HEADERS  += nectareditormainwindow.h \
    Gui/optionswindow.h \
    Core/projectmanager.h \
    Core/project.h \
    Gui/projectpropertieswindow.h \
    Gui/newprojectwindow.h \
    Graphics/graphicalview.h \
    Graphics/graphicscene.h \
    Graphics/graphicsview.h \
    Graphics/abstracttool.h \
    Graphics/brushtool.h \
    Graphics/groundmaterial.h \
    Graphics/basicmaterial.h \
    Graphics/stonematerial.h \
    Graphics/erasertool.h \
    Graphics/sandmaterial.h \
    Graphics/woodmaterial.h \
    Graphics/watermaterial.h \
    Core/materialselector.h \
    Core/PakFileException.hpp \
    Core/Pak.h \
    Widgets/listwidget.h \
    Gui/firststartdialog.h \
    Widgets/gradientwidget.h \
    Core/types.h \
    Core/conversion.h

FORMS    += nectareditormainwindow.ui \
    Gui/optionswindow.ui \
    Gui/projectpropertieswindow.ui \
    Gui/newprojectwindow.ui \
    Gui/firststartdialog.ui

INCLUDEPATH += /home/shen/Bibliothèques/jsoncpp-src-0.5.0/include
LIBS += /home/shen/Bibliothèques/jsoncpp-src-0.5.0/libs/linux-gcc-4.7/libjson_linux-gcc-4.7_libmt.a
