#include "nectareditormainwindow.h"
#include "ui_nectareditormainwindow.h"

NectarEditorMainWindow::NectarEditorMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::NectarEditorMainWindow),
    m_configurationFile( CONFIGURATION_FILE, QSettings::IniFormat ),
    m_projectManager( NULL ),
    m_softwareName( "Nectar Editor 0.3" ),
    m_graphicContext( NULL )
{
    ui->setupUi(this);
    int argc = QApplication::arguments().size();
    QString filepath = "";
    if( argc > 1 )
    {
        filepath = QApplication::arguments().at( 1 );
    }

    setWindowIcon( QIcon( QPixmap( "data/resources/nectar_main_icon.png" ) ) );
    setWindowTitle( m_softwareName );
    readOptions();

    buildInterface();
    createToolbar();
    createDockWidget();
    createStatusBar();
    initEvents();

    //If there is a argument
    if( argc > 1 )
    {
        openProject( filepath );
    }
}

NectarEditorMainWindow::~NectarEditorMainWindow()
{
    saveOptions();
    delete ui;
}

void NectarEditorMainWindow::closeEvent( QCloseEvent *ce )
{
    if( m_projectManager->closeProject() )
        ce->accept();
    else
        ce->ignore();
}

void NectarEditorMainWindow::readOptions()
{
    bool isFirstStart = m_configurationFile.value( "isFirstStart", true ).toBool();
    QSize windowSize = m_configurationFile.value( "ScreenSize", QSize( 1024, 768 ) ).toSize();
    bool maximized = m_configurationFile.value( "IsMaximized", false ).toBool();

    m_configurationFile.beginGroup( "Nectar" );
    QString path = m_configurationFile.value( "NectarPath", "" ).toString();
    m_configurationFile.endGroup();

    m_configurationFile.beginGroup( "NectarEditor" );
    if( isFirstStart )
    {
        QString workspace( QStandardPaths::writableLocation( QStandardPaths::DocumentsLocation ) + "/NectarEditor/projects/" );
        /*FirstStartDialog fsd( 0, &workspace );
        fsd.exec();*/
        m_configurationFile.setValue( "WorkspacePath", workspace );
        createWorkspace();
    }
    m_workspacePath = m_configurationFile.value( "WorkspacePath", QStandardPaths::writableLocation( QStandardPaths::DocumentsLocation ) + "/NectarEditor/projects/" ).toString();

    m_configurationFile.endGroup();

    //Applying parameters
    if( maximized )
        this->setWindowState(Qt::WindowMaximized);
    else
        this->resize( windowSize );
    //Setting isFirstStart to false
    m_configurationFile.setValue( "isFirstStart", false );
    m_projectManager = new ProjectManager( path );
}

void NectarEditorMainWindow::saveOptions()
{
    QSize windowSize = this->size();
    bool isMaximized = this->isMaximized();

    m_configurationFile.setValue( "ScreenSize", windowSize );
    m_configurationFile.setValue( "IsMaximized", isMaximized );
}

void NectarEditorMainWindow::createWorkspace( )
{
    QString basePath = QStandardPaths::writableLocation( QStandardPaths::DocumentsLocation );
    QString nePath = "NectarEditor/projects/";

    QDir workspaceDir( basePath );
    if( workspaceDir.exists() )
    {
        if( !workspaceDir.exists( nePath ) )
        {
            workspaceDir.mkpath( nePath );
        }
    }
}

void NectarEditorMainWindow::buildInterface()
{
    QMenuBar *menuBar = this->menuBar();

    m_mainMenu = menuBar->addMenu( tr( "Nectar Editor" ) );
    m_editMenu = menuBar->addMenu( tr( "Edit" ) );
    m_projectMenu = menuBar->addMenu( tr( "Project" ) );
    m_toolsMenu = menuBar->addMenu( tr( "Tools") );
    m_helpMenu = menuBar->addMenu( "?" );

    m_newLevelAction = m_mainMenu->addAction( tr( "New level" ) );
    m_newLevelAction->setShortcut( QKeySequence( "Ctrl+N" ) );
    m_openLevelAction = m_mainMenu->addAction( tr( "Open a level" ) );
    m_openLevelAction->setShortcut( QKeySequence( "Ctrl+O" ) );
    m_mainMenu->addSeparator();
    m_saveAction = m_mainMenu->addAction( tr( "Save" ) );
    m_saveAction->setShortcut( QKeySequence( "Ctrl+S" ) );
    m_saveAsAction = m_mainMenu->addAction( tr( "Save As" ) + "..." );
    m_saveAsAction->setShortcut( QKeySequence( "Ctrl+Alt+S" ) );
    m_mainMenu->addSeparator();
    m_quitAction = m_mainMenu->addAction( tr( "Quit Nectar Editor" ) );

    m_undoAction = m_editMenu->addAction( tr( "Undo" ) );
    m_redoAction = m_editMenu->addAction( tr( "Redo" ) );

    m_projectPropertiesAction = m_projectMenu->addAction( tr( "Project properties" ) );
    m_projectPropertiesAction->setShortcut( QKeySequence ( "Ctrl+T" ) );
    m_projectPropertiesAction->setEnabled( m_projectManager->isProjectOpen() );
    m_projectMenu->addSeparator();    
    m_buildAction = m_projectMenu->addAction( tr( "Build level" ) );
    m_projectMenu->addSeparator();
    m_buildAction->setShortcut( QKeySequence( "Ctrl+B" ) );
    m_buildAction->setToolTip( tr( "Build the level into a Nel file which be locate in nectar maps" ) );
    m_buildAction->setEnabled( m_projectManager->isProjectOpen() );

    //Building recentProjects part
    onRecentProjectsChanged( m_projectManager->getRecentProjects() );

    m_optionsAction = m_toolsMenu->addAction( tr( "Options" ) );

    m_aboutQtAction = m_helpMenu->addAction( tr( "About Qt" ) );
    m_aboutAction = m_helpMenu->addAction( tr( "About Nectar Editor" ) );

    m_newLevelAction->setIcon( QIcon( QPixmap( "data/resources/filenew.png" ) ) );
    m_openLevelAction->setIcon( QIcon( QPixmap( "data/resources/project_open.png" ) ) );
    m_saveAction->setIcon( QIcon( QPixmap( "data/resources/filesave.png" ) ) );
    m_saveAsAction->setIcon( QIcon( QPixmap( "data/resources/filesaveas.png" ) ) );

    m_undoAction->setIcon( QIcon( QPixmap( "data/resources/undo.png" ) ) );
    m_redoAction->setIcon( QIcon( QPixmap( "data/resources/redo.png" ) ) );

    m_projectPropertiesAction->setIcon( QIcon( QPixmap( "data/resources/project_properties.png" ) ) );
    m_buildAction->setIcon( QIcon( QPixmap( "data/resources/build.png" ) ) );

    m_optionsAction->setIcon( QIcon( QPixmap( "data/resources/options.png" ) ) );
}

void NectarEditorMainWindow::createToolbar()
{
    QToolBar *toolbar = addToolBar( tr( "Main tool bar" ) );

    toolbar->addAction( m_newLevelAction );
    toolbar->addAction( m_openLevelAction );

    toolbar->addSeparator();

    toolbar->addAction( m_saveAction );

    toolbar->addSeparator();

    toolbar->addAction( m_buildAction );


}

void NectarEditorMainWindow::createDockWidget()
{
    m_dockTools = addToolBar( tr( "TooBar") );
    //m_dockTools->setAllowedAreas( Qt::LeftToolBarArea | Qt::RightToolBarArea );

    m_pointerAction = new QAction( QIcon( QPixmap( "data/resources/pointer.png" ) ), tr( "Selector" ), m_dockTools );
    m_brushAction = new QAction( QIcon( QPixmap( "data/resources/brush.png" ) ), tr( "Brush" ), m_dockTools );
    m_eraserAction = new QAction( QIcon( QPixmap( "data/resources/eraser.png" ) ) , tr( "Eraser" ), m_dockTools );
    m_fillAction = new QAction( QIcon( QPixmap( "data/resources/fill.png" ) ), tr( "Fill" ), m_dockTools );
    m_colorPickerAction = new QAction( QIcon( QPixmap( "data/resources/color_picker.png" ) ), tr( "Color picker" ), m_dockTools );

    m_brushAction->setShortcut( QKeySequence( "B" ) );
    m_eraserAction->setShortcut( QKeySequence( "E" ) );

    m_pointerAction->setCheckable( true );
    m_brushAction->setCheckable( true );
    m_eraserAction->setCheckable( true );
    m_fillAction->setCheckable( true );

    disableTools();

    QMap<QString, int> list = m_materialSelector.getMaterialList();

    m_textureChooser = new QComboBox( m_dockTools );
    for( QMap<QString, int>::iterator it = list.begin(); it != list.end(); ++it )
    {
        m_textureChooser->addItem( it.key(), it.value() );
    }
    /*m_textureChooser->addItem( tr( "Void" ), 0 );
    m_textureChooser->addItem( tr( "Ground" ), 1 );
    m_textureChooser->addItem( tr( "Stone" ), 2 );
    m_textureChooser->addItem( tr( "Sand" ), 3 );
    m_textureChooser->addItem( tr( "Wood" ), 4 );
    m_textureChooser->addItem( tr( "Water" ), 5 );*/

    QLabel *label = new QLabel( tr( "Texture" ) );

    m_dockTools->addAction( m_pointerAction );
    m_dockTools->addAction( m_brushAction );
    m_dockTools->addAction( m_eraserAction );
    m_dockTools->addAction( m_fillAction );
    m_dockTools->addAction( m_colorPickerAction );
    m_dockTools->addSeparator();
    m_dockTools->addWidget( label );
    m_dockTools->addWidget( m_textureChooser );
}

void NectarEditorMainWindow::createStatusBar()
{
    QStatusBar *bar = statusBar();
    m_zoomFactorLabel = new QLabel( "Zoom: ", this );
    m_windowCoordsLabel = new QLabel( "Win", this );
    m_sceneCoordsLabel = new QLabel( "Scene", this );

    bar->addPermanentWidget( m_windowCoordsLabel );
    bar->addPermanentWidget( m_sceneCoordsLabel );
    bar->addPermanentWidget( m_zoomFactorLabel );
}

void NectarEditorMainWindow::initEvents()
{
    connect( m_optionsAction, SIGNAL( triggered() ), this, SLOT( openOptionWindow()) );
    connect( m_aboutQtAction, SIGNAL( triggered() ), qApp, SLOT( aboutQt() ) );
    connect( m_quitAction, SIGNAL( triggered() ), qApp, SLOT( quit() ) );
    connect( m_openLevelAction, SIGNAL( triggered() ), this, SLOT( onOpenProjectClicked() ) );
    connect( m_projectManager, SIGNAL( recentsProjectsChanged(QStringList) ), this, SLOT( onRecentProjectsChanged(QStringList) ) );
    connect( m_projectPropertiesAction, SIGNAL( triggered() ), this, SLOT( openProjectProperties() ) );
    connect( m_newLevelAction, SIGNAL( triggered() ), this, SLOT( createNewProject() ) );
    connect( m_brushAction, SIGNAL( triggered() ), this, SLOT( onBrushSelected() ) );
    connect( m_eraserAction, SIGNAL( triggered() ), this, SLOT( onEraserSelected() ) );
    connect( m_textureChooser, SIGNAL( currentIndexChanged(QString) ), this, SLOT( onMaterialChoosed( QString ) ) );    
    connect( m_saveAction, SIGNAL( triggered() ), this, SLOT( onSave() ) );
    connect( m_buildAction, SIGNAL( triggered() ) , this, SLOT( onBuild() ) );
}

void NectarEditorMainWindow::openOptionWindow()
{
    OptionsWindow opt;
    opt.exec();

    QString nectarPath = m_configurationFile.value( "Nectar/NectarPath", "" ).toString();

    //We only change the projects root value if no projects are open
    if( !m_projectManager->isProjectOpen() )
    {
        m_projectManager->setNectarPath( nectarPath );
    }
}

void NectarEditorMainWindow::onOpenProjectClicked()
{
    QString mapsPath = "";

    if( !m_projectManager->getNectarPath().isEmpty() )
    {
        QDir directory(  m_projectManager->getNectarPath() );
        //Trying to go into data/maps
        if( directory.cd( "data/maps" ) )
        {
            mapsPath = directory.filePath( "" );
        }
        else //If the cd fails, we fallback onto getNectarPath()
        {
            mapsPath = m_projectManager->getNectarPath();
        }
    }

    QString projectFile = QFileDialog::getOpenFileName( this, tr( "Select an existing level" ), m_workspacePath, tr( "Nectar Project" ) + " (*.nep)" );

    if( projectFile != "" )
    {
        openProject( projectFile );
    }
}

void NectarEditorMainWindow::openProject( QString path )
{
    if( m_projectManager->isProjectOpen() )
    {
        m_projectManager->closeProject();
        removeProjectEvents();
    }

    m_projectManager->openProject( path );

    if( m_projectManager->isProjectOpen() )
    {
        QString title = m_projectManager->getCurrentProject()->getProjectName();
        title = title + " - " + m_softwareName;
        setWindowTitle( title );
        m_projectPropertiesAction->setEnabled( m_projectManager->isProjectOpen() );
        createGraphicContext();
        enableTools();
        initProjectEvents();

        m_projectManager->getCurrentProject()->setMapDataPointer( m_graphicContext->getScene()->getLevelGrid() );
        GraphicScene *scene = m_graphicContext->getScene() ;
        m_projectManager->getCurrentProject()->syncMapData( scene );
        onZoomFactorChanged( 1.0f );

        scene->setBackgroundListPointer( m_projectManager->getCurrentProject()->backgrounds() );
    }
}

void NectarEditorMainWindow::onRecentProjectsChanged( QStringList projects )
{
    freeRecentsProjectsActions();

    for( int i = 0; i < projects.size(); ++i )
    {
        QAction *action = new QAction( projects.at( i ), this );
        action->setCheckable( true );
        if( m_projectManager->isProjectOpen( projects.at( i ) ) )
        {
            std::cout << "plop" << std::endl;
            action->setChecked( true );
            action->setEnabled( false );
        }

        m_recentProjects.push_front( action );
        m_projectMenu->addAction( action );

        connect( action, SIGNAL( triggered() ), this, SLOT( recentProjectClicked() ) );
    }
}

void NectarEditorMainWindow::freeRecentsProjectsActions()
{
    for( QVector<QAction*>::iterator it = m_recentProjects.begin(); it != m_recentProjects.end(); ++it)
    {
        (*it)->disconnect( SIGNAL( triggered() ) );
        m_projectMenu->removeAction( *it );
    }

    m_recentProjects.clear();
}

void NectarEditorMainWindow::recentProjectClicked()
{
    //Getting sender
    QAction *action = qobject_cast<QAction*>( sender() );

    QString path = action->text();

    openProject( path );
    //Uncheck all other actions
    for( QVector<QAction*>::iterator it = m_recentProjects.begin(); it != m_recentProjects.end(); ++it )
    {
        (*it)->setChecked( false );
        (*it)->setEnabled( true );
    }

    //Check and disable current project
    action->setChecked( true );
    action->setEnabled( false );
}

void NectarEditorMainWindow::openProjectProperties()
{
    ProjectPropertiesWindow ppw( m_projectManager );
    ppw.exec();

    QString title = m_projectManager->getCurrentProject()->getProjectName();
    title += " - " + m_softwareName;
    setWindowTitle( title );

}

void NectarEditorMainWindow::createNewProject()
{
    if( m_projectManager->newProject() )
    {
        QString title = m_projectManager->getCurrentProject()->getProjectName();
        title += " - " + m_softwareName;
        setWindowTitle( title );

        createGraphicContext();
        enableTools();
        initProjectEvents();
        m_projectPropertiesAction->setEnabled( m_projectManager->isProjectOpen() );

        m_projectManager->getCurrentProject()->setMapDataPointer( m_graphicContext->getScene()->getLevelGrid() );

    }
}

void NectarEditorMainWindow::createGraphicContext()
{
    destroyGraphicContext();
    qDebug() <<  m_projectManager->getCurrentProject()->getBlocksNumber() << endl;
    m_graphicContext = new GraphicalView( m_projectManager->getCurrentProject()->getBlocksNumber(), this );
    setCentralWidget( m_graphicContext );
}

void NectarEditorMainWindow::destroyGraphicContext()
{
    if( m_graphicContext != NULL )
    {
        m_graphicContext->deleteLater();
        m_graphicContext = NULL;
    }
}

void NectarEditorMainWindow::onBrushSelected()
{
    QAction *action = qobject_cast<QAction*>( sender() );
    action->setChecked( true );
    m_eraserAction->setChecked( false );
    m_pointerAction->setChecked( false );
    m_fillAction->setChecked( false );
    BrushTool *tool = new BrushTool;
    m_graphicContext->setTool( tool );

    QString materialName = m_textureChooser->currentText();
    BasicMaterial *material = m_materialSelector.getMaterial( materialName );

    if( material != NULL )
        tool->setMaterial( material );

    qDebug() << tool->toolName() << " selected " << endl;
}

void NectarEditorMainWindow::onEraserSelected()
{
    QAction *action = qobject_cast<QAction*>( sender() );
    action->setChecked( true );
    m_brushAction->setChecked( false );
    m_pointerAction->setChecked( false );
    m_fillAction->setChecked( false );
    EraserTool *tool = new EraserTool;
    m_graphicContext->setTool( tool );

    qDebug() << tool->toolName() << " selected" << endl;
}

void NectarEditorMainWindow::enableTools()
{
    m_pointerAction->setEnabled( true );
    m_brushAction->setEnabled( true );
    m_eraserAction->setEnabled( true );
    m_fillAction->setEnabled( true );
    m_colorPickerAction->setEnabled( true );
    m_buildAction->setEnabled( true );
}

void NectarEditorMainWindow::disableTools()
{
    m_pointerAction->setEnabled( false );
    m_brushAction->setEnabled( false );
    m_eraserAction->setEnabled( false );
    m_fillAction->setEnabled( false );
    m_colorPickerAction->setEnabled( false );
    m_buildAction->setEnabled( false );
}


void NectarEditorMainWindow::onMaterialChoosed( QString name )
{
    if( m_graphicContext->getTool() != NULL )
    {
        BrushTool *tool = dynamic_cast<BrushTool*>( m_graphicContext->getTool() );
        BasicMaterial *material = m_materialSelector.getMaterial( name );
        if( material != NULL )
        {
            tool->setMaterial( material );
        }
    }
}

void NectarEditorMainWindow::onZoomFactorChanged( float z )
{
    QString strZoom = "Zoom: " + QVariant( z ).toString() + "x";
    m_zoomFactorLabel->setText( strZoom );
}

void NectarEditorMainWindow::initProjectEvents()
{
    connect( m_graphicContext->getView(), SIGNAL( zoomFactorChanged( float ) ), this, SLOT( onZoomFactorChanged( float ) ) );
    connect( m_graphicContext->getView(), SIGNAL( rectangleSelectionEnabled(bool) ), this, SLOT( onRectangleSelectionEnabled( bool ) ) );
    connect( m_graphicContext->getView(), SIGNAL( mousePositionChanged( QPoint, QPointF ) ), this, SLOT( onMouseMoved( QPoint, QPointF ) ) );
}

void NectarEditorMainWindow::removeProjectEvents()
{
    disconnect( m_graphicContext->getView(), SIGNAL( zoomFactorChanged(float) ) );
    disconnect( m_graphicContext->getView(), SIGNAL( rectangleSelectionEnabled(bool) ) );
    disconnect( m_graphicContext->getView(), SIGNAL( mousePositionChanged( QPoint, QPointF ) ) );
}

void NectarEditorMainWindow::onRectangleSelectionEnabled( bool enabled )
{
    if( enabled )
    {
        statusBar()->showMessage( tr( "Selection rectangle enabled" ), 0 );
    }
    else
    {
        statusBar()->clearMessage();
    }
}

void NectarEditorMainWindow::onMouseMoved( QPoint win, QPointF scene )
{
    QString strWin = "Win x: " + QVariant( win.x() ).toString() + ", y: " + QVariant( win.y() ).toString();
    QString strScene = "Scene x: " + QVariant( scene.x() ).toString() + ", y: " + QVariant( scene.y() ).toString();

    m_windowCoordsLabel->setText( strWin );
    m_sceneCoordsLabel->setText( strScene );
}

void NectarEditorMainWindow::onSave()
{
    if( m_projectManager && m_projectManager->isProjectOpen() )
        m_projectManager->getCurrentProject()->save();
}

void NectarEditorMainWindow::onBuild()
{
    if( m_projectManager && m_projectManager->isProjectOpen() )
    {
        m_projectManager->getCurrentProject()->build();
    }
}
