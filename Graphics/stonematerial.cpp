#include "stonematerial.h"

StoneMaterial::StoneMaterial() : BasicMaterial( "Stone", 2 )
{
    m_brush.setColor( QColor( 149, 142, 136 ) );
    m_brush.setStyle( Qt::SolidPattern );
}
