#ifndef SANDMATERIAL_H
#define SANDMATERIAL_H

#include "basicmaterial.h"

class SandMaterial : public BasicMaterial
{
public:
    SandMaterial();
};

#endif // SANDMATERIAL_H
