#include "erasertool.h"

EraserTool::EraserTool() : AbstractTool( "Eraser" )
{
}

void EraserTool::doAction( GraphicScene *scene, QGraphicsView *view, QPoint pos )
{
    QPointF p = view->mapToScene( pos );
    QPoint roundedPoint = calculateRoundedPoint( p );
    QPoint casePos = calculateCasePosition( roundedPoint );

    MultiGraphicRectItemVector *gridArray = scene->getLevelGrid();

    if( (*gridArray)[casePos.x()][casePos.y()] != NULL )
    {
        MapCase *mCase = (*gridArray)[casePos.x()][casePos.y()];

        scene->removeItem( mCase->item );
        delete mCase->item;
        delete mCase;
        mCase->item = NULL;
        (*gridArray)[casePos.x()][casePos.y()] = NULL;
        m_lastPixelDraw = casePos;
    }
}
