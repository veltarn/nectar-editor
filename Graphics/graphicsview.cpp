#include "graphicsview.h"


GraphicsView::GraphicsView( QWidget *parent ) :
    QGraphicsView( parent ),
    m_tool( NULL ),
    m_lmbPressed( false ),
    m_rmbPressed( false ),
    m_shiftPressed( false ),
    m_selectionRectangle( NULL ),
    m_baseZoom( 1.f )
{
    setMouseTracking( true );
    emit zoomFactorChanged( m_baseZoom );
}

GraphicsView::GraphicsView( GraphicScene *scene, QWidget *parent ) :
    QGraphicsView( scene, parent ),
    m_tool( NULL ),
    m_scene( scene ),
    m_lmbPressed( false ),
    m_rmbPressed( false ),
    m_shiftPressed( false ),
    m_selectionRectangle( NULL ),
    m_baseZoom( 1.f )
{
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    //setFocusPolicy( Qt::NoFocus );
    //initEvents();

    setMouseTracking( true );
    emit zoomFactorChanged( m_baseZoom );
}

void GraphicsView::mousePressEvent(QMouseEvent *event)
{
    if( m_tool != NULL && event->button() == Qt::LeftButton )
    {
        if( m_shiftPressed )
        {
            QSizeF rectSize = m_selectionRectangle->rect().size() / 16.f;
            QPoint origPoint;
            origPoint.setX( m_selectionRectangle->rect().left() );
            origPoint.setY( m_selectionRectangle->rect().top() );

            for( int y = 0; y < rectSize.height(); ++y )
            {
                for( int x = 0; x < rectSize.width(); ++x )
                {
                    QPointF cPos;
                    QPoint windowPos;
                    cPos.setX( origPoint.x() + ( 16.f * x ) );
                    cPos.setY( origPoint.y() + ( 16.f * y ) );
                    windowPos = this->mapFromScene( cPos );

                    m_tool->doAction( m_scene, this, windowPos );
                }
            }
        }
        else
        {
            m_lmbPressed = true;
            qDebug() << event->pos() << endl;
            m_tool->doAction( m_scene, this, event->pos() );
        }
    } else if ( event->button() == Qt::RightButton ) {
        m_rmbPressed = true;
        QApplication::setOverrideCursor( Qt::ClosedHandCursor );
        m_lastCursorPosition = event->pos();
    }
}

void GraphicsView::mouseReleaseEvent( QMouseEvent *event )
{
    if( event->button() == Qt::LeftButton )
    {
        m_lmbPressed = false;
    } else if ( event->button() == Qt::RightButton ) {
        m_rmbPressed = false;
        QApplication::restoreOverrideCursor();
    }
}

void GraphicsView::setTool( AbstractTool *tool )
{
    if( m_tool != NULL )
    {
        qDebug() << "New tool selected ( " << tool->toolName() << " ) deleting current tool ( " << m_tool->toolName() << " )" << endl;
        delete m_tool;
        m_tool = NULL;
    }

    m_tool = tool;
}

AbstractTool *GraphicsView::getTool() const
{
    return m_tool;
}

void GraphicsView::initEvents()
{
    //connect( &m_timer, SIGNAL( timeout() ), this, SLOT( onShot() ) );
}

void GraphicsView::mouseMoveEvent( QMouseEvent *event )
{
    if( m_lmbPressed )
    {
        QPoint pos = this->mapFromGlobal( QCursor::pos() );

        m_tool->doAction( m_scene, this, pos );
    } else if( m_rmbPressed ) {
        QPoint deltaPos = event->pos() - m_lastCursorPosition;

        m_lastCursorPosition = event->pos();
        verticalScrollBar()->setValue( verticalScrollBar()->value() - deltaPos.y() );
        horizontalScrollBar()->setValue( horizontalScrollBar()->value() - deltaPos.x() );
    } else if( m_shiftPressed ) {
        if( m_tool != NULL && ( m_tool->toolName() == "Brush" || m_tool->toolName() == "Eraser" ) )
        {            
            QPoint origPos = m_tool->getLastPixelDraw();
            QPointF cPosF = this->mapToScene( this->mapFromGlobal( QCursor::pos() ) );
            QPoint cPos;

            cPos.setX( cPosF.x() - static_cast<int>( cPosF.x() ) % 16 );
            cPos.setY( cPosF.y() - static_cast<int>( cPosF.y() ) % 16 );

            cPos = cPos / 16.f;

            /*int width = abs( cPos.x() - tool->getLastPixelDraw().x() );
            int height = abs( cPos.y() - tool->getLastPixelDraw().y() );*/
            int width = cPos.x() - m_tool->getLastPixelDraw().x();
            int height = cPos.y() - m_tool->getLastPixelDraw().y();
            /*QRectF rectangle( tool->getLastPixelDraw(), QSizeF( width, height ) );
            qDebug() << rectangle << endl;*/
            //qDebug() << cPosF << " / " << width << " x " << height << endl;
            if( width < 0 && height < 0 ) //TLC
            {
                m_selectionRectangle->setRect( cPos.x() * 16.f, cPos.y() * 16.f, ( abs( width ) + 1 ) * 16.f, ( abs( height ) + 1 ) * 16.f );
            } else if( width > 0 && height < 0 ) { // TRC
                cPos.setX( origPos.x() );
                m_selectionRectangle->setRect( cPos.x() * 16.f, cPos.y() * 16.f, width * 16.f, ( abs( height ) + 1 ) * 16.f );
            } else if( width < 0 && height > 0 ) { // BLC
                cPos.setY( origPos.y() );
                m_selectionRectangle->setRect( cPos.x() * 16.f, cPos.y() * 16.f, ( abs( width ) + 1 ) * 16.f, height * 16.f );
            }
            else //BRC
            {
                m_selectionRectangle->setRect( origPos.x() * 16.f, origPos.y() * 16.f, width * 16.f, height * 16.f );
            }

        }
    }

    QPoint pos = QCursor::pos();
    mousePositionChanged( this->mapFromGlobal( pos ), this->mapToScene( this->mapFromGlobal( pos ) ) );
}


void GraphicsView::wheelEvent( QWheelEvent *we )
{
    qDebug() << "plop" << endl;
    float scaleFactor = ( ( we->angleDelta().y() / 8.f ) / 100.f ) + m_baseZoom;

    zoom( scaleFactor );
}

void GraphicsView::keyPressEvent( QKeyEvent *event )
{
    if( event->key() == Qt::Key_Shift )
    {
        if( m_tool != NULL && ( m_tool->toolName() == "Brush" || m_tool->toolName() == "Eraser" ) )
        {
            //BrushTool *tool = dynamic_cast<BrushTool*>( m_tool );
            qDebug() << m_tool->getLastPixelDraw() << endl;
            if( m_tool->getLastPixelDraw() != QPoint( -1, -1 ) )
            {
                QPoint lastPixelDraw = m_tool->getLastPixelDraw();
                m_shiftPressed = true;
                rectangleSelectionEnabled( true );
                m_selectionRectangle = new QGraphicsRectItem( QRectF( lastPixelDraw .x() * 16.f, lastPixelDraw .y() * 16.f, 16, 16 ) );
                m_selectionRectangle->setBrush( QBrush( QColor( 255, 92, 92, 200 ) ) );
                this->scene()->addItem( m_selectionRectangle );
            }
        }
    }
}

void GraphicsView::keyReleaseEvent( QKeyEvent *event )
{
    if( event->key() == Qt::Key_Shift )
    {
        if( m_tool != NULL && ( m_tool->toolName() == "Brush" || m_tool->toolName() == "Eraser" ) )
        {
            m_shiftPressed = false;
            rectangleSelectionEnabled( false );

            if( m_selectionRectangle != NULL )
            {
                this->scene()->removeItem( m_selectionRectangle );
                delete m_selectionRectangle;
                m_selectionRectangle = NULL;
            }
        }
    }
}

void GraphicsView::zoom( float scaleFactor )
{
    QRectF r( 0, 0, 1, 1 );

    qreal factor = transform().scale( scaleFactor, scaleFactor ).mapRect( r ).width();
    qDebug() << factor << endl;
    //Verifying if we zoomed more than 5x or -5x
    if( factor > 5  || factor < 0.149827f )
        return;

    scale( scaleFactor, scaleFactor );
    emit zoomFactorChanged( factor );
}
