#include "groundmaterial.h"

GroundMaterial::GroundMaterial() : BasicMaterial( "Ground", 1 )
{
    m_brush.setColor( QColor( 14, 125, 3 ) );
    m_brush.setStyle( Qt::SolidPattern);
}

GroundMaterial::~GroundMaterial()
{

}
