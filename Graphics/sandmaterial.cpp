#include "sandmaterial.h"

SandMaterial::SandMaterial() : BasicMaterial( "Sand", 3 )
{
    m_brush.setColor( QColor( 255, 226, 99 ) );
    m_brush.setStyle( Qt::SolidPattern );
}
