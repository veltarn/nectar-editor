#ifndef ABSTRACTTOOL_H
#define ABSTRACTTOOL_H

#include <cmath>
#include <QApplication>
#include <QGraphicsView>
#include <QPoint>
#include <QCursor>
#include "graphicscene.h"

class AbstractTool
{
public:
    AbstractTool( QString toolName );
    virtual ~AbstractTool();

    //Do the action the tool is specialized for
    virtual void doAction( GraphicScene *scene, QGraphicsView *view, QPoint pos ) = 0;

    virtual QString toolName() const;

    virtual QPoint getLastPixelDraw() const;
    virtual void resetLastPixelDraw(); //Intéret?

protected:
    QPoint calculateRoundedPoint( QPointF p );
    QPoint calculateCasePosition( QPoint p );
protected:
    QString m_toolName;

    QPoint m_lastPixelDraw;
};

#endif // ABSTRACTTOOL_H
