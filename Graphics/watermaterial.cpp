#include "watermaterial.h"

WaterMaterial::WaterMaterial() : BasicMaterial( "Water", 5 )
{
    m_brush.setColor( QColor( 47, 150, 216 ) );
    m_brush.setStyle( Qt::SolidPattern );
}
