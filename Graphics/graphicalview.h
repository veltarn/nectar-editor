#ifndef GRAPHICALVIEW_H
#define GRAPHICALVIEW_H

#include <cmath>
#include <QDebug>
#include <QWidget>
#include <QVBoxLayout>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QKeyEvent>
#include "abstracttool.h"
#include "graphicscene.h"
#include "graphicsview.h"

class GraphicalView : public QWidget
{
public:
    GraphicalView( QSize worldSize, QWidget *parent = 0 );

    //void zoom( float scaleFactor );
    void setTool( AbstractTool *tool );
    AbstractTool *getTool() const;
    GraphicScene *getScene() const;
    GraphicsView *getView() const;
protected:
    //void wheelEvent( QWheelEvent * );
private:
    void buildInterface( QSize worldSize );
    void initEvents();

private:
    GraphicsView *m_view;
    GraphicScene *m_scene;
    QVBoxLayout *m_mainLayout;

};

#endif // GRAPHICALVIEW_H
