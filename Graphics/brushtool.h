#ifndef BRUSHTOOL_H
#define BRUSHTOOL_H

#include <QDebug>
#include <QGraphicsRectItem>
#include <QBrush>
#include <QColor>
#include "abstracttool.h"
#include "basicmaterial.h"

class BrushTool : public AbstractTool
{
public:
    BrushTool();

    virtual void doAction( GraphicScene *scene, QGraphicsView *view, QPoint pos );
    void setMaterial( BasicMaterial *material );

private:
    BasicMaterial *m_material;

};

#endif // BRUSHTOOL_H
