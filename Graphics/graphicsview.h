#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QDebug>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QGraphicsRectItem>
#include <QScrollBar>
#include <QKeyEvent>
#include <QTimer>
#include <QCursor>
#include "brushtool.h"
#include "erasertool.h"
#include "graphicscene.h"


class GraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    GraphicsView( QWidget *parent = 0 );
    GraphicsView( GraphicScene *scene, QWidget *parent = 0 );

    void setTool( AbstractTool *tool );
    AbstractTool *getTool() const;
    void zoom( float scaleFactor );

signals:
    void zoomFactorChanged( float );
    void scrollChanged( float, float );
    void rectangleSelectionEnabled( bool );
    void mousePositionChanged( QPoint, QPointF );
public slots:

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent( QMouseEvent *event );
    void mouseMoveEvent( QMouseEvent *event );
    void keyPressEvent( QKeyEvent *event );
    void keyReleaseEvent( QKeyEvent *event );
    void wheelEvent( QWheelEvent *event );
private:
    void initEvents();
private:
    AbstractTool *m_tool;
    GraphicScene *m_scene;

    bool m_lmbPressed;
    bool m_rmbPressed;
    bool m_shiftPressed;

    QGraphicsRectItem *m_selectionRectangle;

    QPoint m_lastCursorPosition;
    const int m_baseZoom;
};

#endif // GRAPHICSVIEW_H
