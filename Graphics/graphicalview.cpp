#include "graphicalview.h"

GraphicalView::GraphicalView(QSize casesNumber, QWidget *parent ) : QWidget( parent )
{
    setMouseTracking( true );
    buildInterface( casesNumber );
}

void GraphicalView::buildInterface( QSize casesNumber)
{
    m_scene = new GraphicScene( casesNumber, this );
    m_view = new GraphicsView( m_scene, this );
    //m_view->setDragMode( QGraphicsView::ScrollHandDrag );
    m_mainLayout = new QVBoxLayout();

    m_mainLayout->addWidget( m_view );
    setLayout( m_mainLayout );
}

/*void GraphicalView::wheelEvent( QWheelEvent *we )
{
    qDebug() << "plop" << endl;
    float scaleFactor = ( ( we->angleDelta().y() / 8.f ) / 100.f ) + m_baseZoom;

    zoom( scaleFactor );
}

void GraphicalView::zoom( float scaleFactor )
{
    QRectF r( 0, 0, 1, 1 );

    qreal factor = m_view->transform().scale( scaleFactor, scaleFactor ).mapRect( r ).width();
    qDebug() << factor << endl;
    //Verifying if we zoomed more than 5x or -5x
    if( factor > 5  || factor < 0.149827f )
        return;

    m_view->scale( scaleFactor, scaleFactor );
}*/

void GraphicalView::setTool( AbstractTool *tool )
{
    m_view->setTool( tool );
}

AbstractTool *GraphicalView::getTool() const
{
    return m_view->getTool();
}

GraphicsView *GraphicalView::getView() const
{
    return m_view;
}

GraphicScene *GraphicalView::getScene() const
{
    return m_scene;
}
