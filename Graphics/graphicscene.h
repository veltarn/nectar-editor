#ifndef GRAPHICSCENE_H
#define GRAPHICSCENE_H

#include <cmath>
#include <QDebug>
#include <QGraphicsScene>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QBrush>
#include <QSize>
#include <QVector>
#include <QList>
#include <QMap>
#include <QPixmap>

struct MapCase
{
    int caseType;
    QGraphicsRectItem *item;
};

static const int GRID_STEP = 16;

typedef QVector< QVector< MapCase* > > MultiGraphicRectItemVector;

class GraphicScene : public QGraphicsScene
{
public:
    GraphicScene( QSize casesNumber, QObject *parent = 0 );
    MultiGraphicRectItemVector *getLevelGrid();

    void setBackgroundListPointer( QStringList *backgrounds );

protected:
    void drawBackground( QPainter *painter, const QRectF &rect );
    //void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    //void mousePressEvent(QGraphicsSceneMouseEvent *event);
    /*void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);*/

    QSize getCasesNumber() const;
private:
    qreal round( qreal val, int step );
private:
    QSize m_casesNumber;
    bool m_mousePress;
    MultiGraphicRectItemVector m_grid;
    QStringList *m_backgroundList;

};

#endif // GRAPHICSCENE_H
