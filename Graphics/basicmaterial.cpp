#include "basicmaterial.h"

BasicMaterial::BasicMaterial( QString name, unsigned int id ) : QBrush(), m_brush(), m_materialName( name ), m_id( id )
{
}

BasicMaterial::~BasicMaterial()
{

}

QBrush BasicMaterial::getBrush() const
{
    return m_brush;
}

QString BasicMaterial::materialName() const
{
    return m_materialName;
}


unsigned int BasicMaterial::materialId() const
{
    return m_id;
}
