#include "woodmaterial.h"

WoodMaterial::WoodMaterial() : BasicMaterial( "Wood", 4 )
{
    m_brush.setColor( QColor( 125, 84, 45 ) );
    m_brush.setStyle( Qt::SolidPattern );
}
