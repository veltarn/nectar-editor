#ifndef WOODMATERIAL_H
#define WOODMATERIAL_H

#include "basicmaterial.h"

class WoodMaterial : public BasicMaterial
{
public:
    WoodMaterial();
};

#endif // WOODMATERIAL_H
