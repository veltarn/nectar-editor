#include "abstracttool.h"

AbstractTool::AbstractTool( QString toolName ) : m_toolName( toolName ), m_lastPixelDraw( -1, -1 )
{
}

AbstractTool::~AbstractTool()
{
    QApplication::restoreOverrideCursor();
}

QString AbstractTool::toolName() const
{
    return m_toolName;
}


QPoint AbstractTool::calculateRoundedPoint( QPointF p )
{
    QPoint roundedPoint;
    p.setX( round( p.x() ) );
    p.setY( round( p.y() ) );

    int modX = static_cast<int>( p.x() ) % 16;
    int modY = static_cast<int>( p.y() ) % 16;

    if( modX != 0 )
    {
        /*if( modX >= 1 && modX < 8 )
        {
            modX = 16 - ( 16 - 1 );
        }
        else */if( modX > 13 && modX < 16 ) //Error rate correction due to mapToScene method
        {
            modX = ( ( 16 / 2 ) - ( 16 / 2 - ( 16 - modX ) ) ) * -1;
        }
    }

    if( modY != 0 )
    {
        /*if( modY >= 1 && modY < 8 )
        {
            modY = 16 - ( 16 - 1 );
        } else */if( modY > 13 && modY < 16 ) { //Error rate correction due to mapToScene method
            modY = ( ( 16 / 2) - ( 16 / 2 - ( 16 - modY ) ) ) * -1;
        }
    }

    roundedPoint.setX( p.x() - modX );
    roundedPoint.setY( p.y() - modY );

    return roundedPoint;
}

QPoint AbstractTool::calculateCasePosition( QPoint p )
{
    return ( p / 16 );
}


QPoint AbstractTool::getLastPixelDraw() const
{
    return m_lastPixelDraw;
}


void AbstractTool::resetLastPixelDraw()
{
    m_lastPixelDraw = QPoint( -1, -1 );
}
