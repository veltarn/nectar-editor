#ifndef STONEMATERIAL_H
#define STONEMATERIAL_H

#include "basicmaterial.h"

class StoneMaterial : public BasicMaterial
{
public:
    StoneMaterial();
};

#endif // STONEMATERIAL_H
