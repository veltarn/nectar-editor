#include "brushtool.h"

BrushTool::BrushTool() : AbstractTool( "Brush" ), m_material( NULL )
{
    QCursor cursor( QPixmap( "data/resources/cursor/BasicBrush.png" ) );

    QApplication::setOverrideCursor( cursor );
}

void BrushTool::doAction(GraphicScene *scene, QGraphicsView *view, QPoint pos )
{
    QPointF p = view->mapToScene( pos );
    QPoint roundedPoint = calculateRoundedPoint( p );

    QPoint casePos = calculateCasePosition( roundedPoint );

    MultiGraphicRectItemVector *gridArray = scene->getLevelGrid();
    qDebug() << casePos << " => " << p << ", " << pos << endl;
    if( (*gridArray)[casePos.x()][casePos.y()] == NULL && m_material != NULL )
    {
        MapCase *mCase = new MapCase;
        QGraphicsRectItem *item = new QGraphicsRectItem( QRectF( roundedPoint.x(), roundedPoint.y(), 16, 16 ) );

        mCase->caseType = m_material->materialId();
        mCase->item = item;

        item->setBrush( m_material->getBrush() );
        scene->addItem( item );
        (*gridArray)[casePos.x()][casePos.y()] = mCase;

        m_lastPixelDraw = casePos;
    }
    else if( (*gridArray)[casePos.x()][casePos.y()] != NULL && m_material != NULL )
    {
        //We have to replace current material by the current one
        MapCase *mCase = (*gridArray)[casePos.x()][casePos.y()];
        QGraphicsRectItem *cItem = mCase->item;
        scene->removeItem( cItem );
        delete cItem;
        delete mCase;

        mCase = NULL;
        cItem = NULL;

        mCase = new MapCase;
        QGraphicsRectItem *item = new QGraphicsRectItem( QRectF( roundedPoint.x(), roundedPoint.y(), 16, 16 ) );

        item->setBrush( m_material->getBrush() );
        scene->addItem( item );
        mCase->caseType = m_material->materialId();
        mCase->item = item;
        (*gridArray)[casePos.x()][casePos.y()] = mCase;

        m_lastPixelDraw = casePos;
    }
}

void BrushTool::setMaterial( BasicMaterial *material )
{
    if( m_material != NULL)
    {
        delete m_material;
        m_material = NULL;
    }

    m_material = material;
}


