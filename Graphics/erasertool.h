#ifndef ERASERTOOL_H
#define ERASERTOOL_H

#include "groundmaterial.h"
#include "stonematerial.h"
#include "abstracttool.h"

class EraserTool : public AbstractTool
{
public:
    EraserTool();

    virtual void doAction( GraphicScene *scene, QGraphicsView *view, QPoint pos );
};

#endif // ERASERTOOL_H
