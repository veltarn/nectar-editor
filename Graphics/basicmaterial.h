#ifndef BASICMATERIAL_H
#define BASICMATERIAL_H

#include <QString>
#include <QBrush>

class BasicMaterial : public QBrush
{
public:
    explicit BasicMaterial( QString name, unsigned int id );
    virtual ~BasicMaterial();

    virtual QBrush getBrush() const;
    virtual QString materialName() const;
    virtual unsigned int materialId() const;
protected:
    QBrush m_brush;
    QString m_materialName;
    unsigned int m_id;
};

#endif // BASICMATERIAL_H
