#include "graphicscene.h"

GraphicScene::GraphicScene( QSize casesNumber, QObject *parent) : QGraphicsScene( QRectF( 0, 0, casesNumber.width() * 16.f, casesNumber.height() * 16.f ), parent ), m_casesNumber( casesNumber ), m_grid( QVector<QVector<MapCase*> >( casesNumber.width(), QVector<MapCase*>( casesNumber.height(), NULL ) ) ), m_backgroundList( NULL )
{
}

void GraphicScene::setBackgroundListPointer( QStringList *backgrounds )
{
    m_backgroundList = backgrounds;
}

void GraphicScene::drawBackground( QPainter *painter, const QRectF &rect )
{
    Q_UNUSED( rect );
    //Drawing background images if exists
    if( m_backgroundList != NULL )
    {

    }

    int step = GRID_STEP;
    painter->setPen( QPen( QColor( 120, 120, 150, 125 ) ) );
    QRectF iSceneRect = sceneRect();

    qreal start = this->round( iSceneRect.top(), GRID_STEP );
    if( start > iSceneRect.top() )
    {
        start -= GRID_STEP;
    }

    //Horizontal Grid
    for( qreal y = start; y < iSceneRect.height() - start; )
    {
        //qDebug() << y << endl;
        y += step;
        painter->drawLine( iSceneRect.left(), y, iSceneRect.right(), y );
    }

    start = this->round( iSceneRect.left(), GRID_STEP );

    if( start > iSceneRect.left() )
    {
        start -= GRID_STEP;
    }

    for( qreal x = start; x < iSceneRect.width() - start; )
    {
        x += step;
        painter->drawLine( x, iSceneRect.top(), x, iSceneRect.bottom() );
    }
}


qreal GraphicScene::round( qreal val, int step )
{
    int tmp = int(val) + step / 2;
    tmp -= tmp % step;
    return qreal(tmp);
}



/*void GraphicScene::mousePressEvent( QGraphicsSceneMouseEvent *me )
{
    QGraphicsView *view = this->views().first();
    QPoint point = view->mapFromScene( me->scenePos() );

    qDebug() << point << " / " << view->mapToScene( point ) << " / " << me->scenePos() << endl;

    if( me->button() == Qt::RightButton )
    {
        QGraphicsRectItem *item = new QGraphicsRectItem( QRectF( me->scenePos().x(), me->scenePos().y(), 16, 16 ) );
        item->setBrush( QBrush( QColor( 65, 124, 70 ) ) );
        this->addItem( item );
    }
}
void GraphicScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *me)
{
    Q_UNUSED( me );
    m_mousePress = false;
}

void GraphicScene::mouseMoveEvent( QGraphicsSceneMouseEvent *me )
{
    if( m_mousePress )
    {
        QList<QGraphicsView*> listViews = views();
        QGraphicsView *view = listViews.at( 0 );

        QPointF delta = me->scenePos() - me->lastScenePos();
        qDebug() << delta << endl;
        view->translate( delta.x() * 100.f, delta.y() * 100.f );
    }
}*/

QSize GraphicScene::getCasesNumber() const
{
    return m_casesNumber;
}

QVector< QVector< MapCase * > > *GraphicScene::getLevelGrid()
{
    return &m_grid;
}
