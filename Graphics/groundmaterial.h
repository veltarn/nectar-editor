#ifndef GROUNDMATERIAL_H
#define GROUNDMATERIAL_H

#include "basicmaterial.h"

class GroundMaterial : public BasicMaterial
{
public:
    explicit GroundMaterial();
    virtual ~GroundMaterial();
};

#endif // GROUNDMATERIAL_H
