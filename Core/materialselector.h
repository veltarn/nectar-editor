#ifndef MATERIALSELECTOR_H
#define MATERIALSELECTOR_H

#include <QMap>
#include <QString>
#include "../Graphics/groundmaterial.h"
#include "../Graphics/stonematerial.h"
#include "../Graphics/sandmaterial.h"
#include "../Graphics/woodmaterial.h"
#include "../Graphics/watermaterial.h"

class MaterialSelector
{
public:
    MaterialSelector();

    QMap<QString, int> getMaterialList() const;
    BasicMaterial *getMaterial( QString name );
    BasicMaterial *getMaterial( int materialId );
private:
    BasicMaterial *m_currentMaterial;
    QMap<QString, int> m_materialList;
};

#endif // MATERIALSELECTOR_H
