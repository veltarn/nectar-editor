#include "zip.h"
#include "miniz.c"

using namespace std;

void Zip::addFile( string file, string nameInArchive ) {
    m_filePaths[file] = nameInArchive;
}

void Zip::removeFile( string file ) {
    for( map<string, string>::iterator it = m_filePaths.begin(); it != m_filePaths.end(); ++it ) {
        if( it->first == file )
        {
            m_filePaths.erase( it );
            break;
        }
    }
}

bool Zip::createArchive( string archiveName ) {
    cout << "Creating archive \"" << archiveName << "\"" << endl;
    cout << m_filePaths.size() << " file(s) to archive" << endl;
    bool opOk = true;
    cout << m_filePaths.size() << endl;
    for( map<string, string>::iterator it = m_filePaths.begin(); it != m_filePaths.end(); ++it ) {
        string source = it->first;
        string dest = it->second;
        QString qSource = source.c_str();
        QFile file( qSource );
        file.open( QIODevice::ReadOnly );
        quint64 size = file.size();

        QByteArray buffer = file.readAll();

        cout << "Adding " << file.fileName().toStdString() << "(" << size << ")" << endl;
        mz_bool ok = mz_zip_add_mem_to_archive_file_in_place( archiveName.c_str(), dest.c_str(), buffer.data(), buffer.size(), "NC", (size_t)strlen("NC"), MZ_BEST_COMPRESSION );
        if ( !ok )
            opOk = false;
    }
    return opOk;
}
