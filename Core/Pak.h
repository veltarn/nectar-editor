#ifndef PAK_H
#define PAK_H

#include <iostream>
#include <cstring>
#include <fstream>
#include <vector>
#include <map>
#include "PakFileException.hpp"
/**
    \struct sPAKHeader
    Structure permettant d'avoir des informations sur le fichier Pak lui même.
*/
struct sPAKHeader
{
    char uniqueID[5]; ///ID Utilisée pour savoir si le fichier Pak provient de cette classe
    char version[3]; ///Version du fichier Pak
    unsigned int nb_files; ///Nombre de fichiers dans l'archive
};

/**
    \struct sFileEntry
    Structure représentant un fichier, elle contient les informations du fichier stocké à savoir son nom, sa taille et sa position dans le fichier Pak
*/
struct sFileEntry
{
    char name[300]; ///Nom du fichier
    long size; ///Taille du fichier
    long offset; ///Position dans des données du fichier dans le fichier Pak à partir du début;
};

/**
    \class PAK
    Classe permettant d'archiver, de désarchiver des fichiers en un seul.
*/
class PAK
{
    public:
        /**
            PAK
            Constructeur
        */
        PAK();

        /**
            ~PAK
            Destructeur
        */
        virtual ~PAK();
        
        /**
         *  Ajoute le fichier fileName à la liste des fichiers à archiver. L'archivage ne se fait PAS encore (utilisation de create() nécessaire
         * 
         * @param fileName nom du fichier source à rajouter
         * @param destinationName Nom éventuel du fichier lorsqu'il sera dans l'archive (si ce paramètre est vide, le fichier prendra le nom de fileName )
         */
        void addFile( std::string fileName, std::string destinationName = "" );
        
        /**
         * Retire le fichier fileName de la liste des fichiers à archiver
         * @param fileName nom du fichier à retirer
         */
        void removeFile( std::string fileName );
        /**
            \brief Crée un fichier Pak à partir d'une liste de fichiers

            Fonction qui permet de créer un fichier Pak à partir d'une liste de fichiers et l'enregistre vers "destination"

            \param files Liste de fichiers
            \param destination Lien vers le fichier de destination sur lequel écrire
         * \throws Exception if files to compress doesn't exists
        */
       void create( std::string destination);

        /**
            \brief Lit un fichier Pak et stocke la liste des fichiers

            Cette fonction lit le fichier Pak passé via le paramétre "source" et stocke la liste des fichier (vecteur de sFileEntry)

            \param Localisation du fichier Pak
        */
        void read(std::string source);

        /**
            \brief Renvoie un pointeur contenant le fichier en mémoire

            Cette fonction extrait et renvoie un pointeur sur le fichier "filename" présent dans le fichier Pak

            \param Nom du fichier que l'on souhaite extraire

            \return Pointeur vers le fichier extrait, NULL si aucun fichier n'a été trouvé
        */
        char *getFile(std::string filename);

        /**
            \brief Renvoie la taille du fichier demandé

            Fonction renvoyant la taille du fichier "filename" en octets

            \param Nom du fichier sur qui on souhaite connaitre la taille
            \return Taille du fichier en octets
        */
        long int getFileSize( std::string filename );
        
        std::vector<sFileEntry> entriesList() const;
        
        std::string getSourcePath( std::string destination );

private:
    std::string filename( std::string filepath ); //Returns only filename
        
    private:
        //              Source      , Destination
        std::map< std::string, std::string > m_filesList; //Liste des fichiers à compacter
        std::string m_pakfileName; ///Nom du fichier Pak
        sPAKHeader m_header; ///Header du fichier Pak;
        std::vector<sFileEntry> m_entries; ///Table des fichiers à intégrer
        char *m_buffer; ///Buffer qui permettra de lire les fichier (pour intégration ou extraction)
};

#endif // PAK_H
