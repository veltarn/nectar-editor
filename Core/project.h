#ifndef PROJECT_H
#define PROJECT_H

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <QDebug>
#include <QObject>
#include <QMessageBox>
#include <QFileDialog>
#include <QString>
#include <QDir>
#include <QSettings>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QFile>
#include <QTemporaryFile>
#include <QSize>
#include <QTextStream>
#include <QMap>
#include <QVector>
#include <QFileInfo>
#include <QProgressDialog>
#include "zip.h"
//#include <json/json.h>
#include "types.h"
#include "conversion.h"
#include "../Graphics/graphicscene.h"
#include "../Graphics/basicmaterial.h"
#include "materialselector.h"

typedef QJsonObject::Iterator QJsonObjectIterator;
class Project : public QObject
{
    Q_OBJECT
public:
    explicit Project(QObject *parent = 0);
    Project( QString path, QObject *parent = 0 );

    void readOptions();

    bool load();
    bool load( QString path );

    void setProjectName( QString name );
    void setProjectAuthor( QString author );
    void setProjectPath( QString path );

    QString getProjectName() const;
    QString getProjectAuthor() const;
    QString getProjectPath() const;
    void setMapDataPointer( MultiGraphicRectItemVector *mapData );

    void syncMapData( GraphicScene *scene );

    unsigned int getMaximumPlayers() const;
    void setMaximumPlayers( unsigned int p );

    void addBackground( QString background );
    void setBackgroundList( QStringList &backgroundList );
    void removeBackground( QString background );
    QStringList::iterator findBackground( QString background );
    QStringList *backgrounds();
    void clearBackgroundList();
    bool backgroundExists( QString background );
    unsigned int backgroundsNumber();

    void setMusicFilePath( QString path );
    QString getMusicFilePath() const;

    void setScreenshotPath( QString path );
    QString getScreenshotPath() const;

    void setSkyGradient( QVector<IndividualColorData> sky );
    QVector<IndividualColorData> getSkyGradient() const;

    QSize getMapSize() const;
    QSize getBlocksNumber() const;
    void setBlocksNumber( QSize num );
    void setBlocksNumber( int x, int y );

    void setModified( bool modified );
    bool isModified() const;

    bool isValid() const;

    void save();
    void build();
    /**
     * @brief writeJsonLevelFile
     * @param buildingMode Used to write file path according to binary file structure or regular folders strucutre (with absolute file path)
     */
    void writeJsonLevelFile( bool building = false );
    QJsonArray writeSkyGradient();

    /**
     * @brief close
     * @return true if the project is closed, false otherwise (if cancel has been clicked)
     */
    bool close();
signals:
    void backgroundAdded( QStringList *backgrounds );
    void backgroundRemoved( QStringList *backgrounds );
public slots:
private:
    QJsonArray writeMap();
    //unsigned int getMaterialType( QGraphicsRectItem *item );
private:
    QJsonDocument m_jsonFile;

    QString m_projectName;
    QString m_projectAuthor;
    QString m_projectPath;

    QString m_nectarPath;

    bool m_isValid;
    bool m_isModified;

    QSize m_mapSize;

    //ceil ( MapSize / 16 )
    QSize m_blockNumber;

    MultiGraphicRectItemVector *m_mapData;
    int m_maximumPlayers;

    QFile m_projectJsonFile; //Used to store content of json File

    QString m_musicPath;
    QString m_screenshotPath;

    QStringList m_backgroundList;

    QVector<IndividualColorData> m_skycolor;
};

#endif // PROJECT_H
