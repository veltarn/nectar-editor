#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <iostream>
#include <QObject>
#include <QString>
#include <QSettings>
#include <assert.h>
#include "project.h"
#include "../Gui/newprojectwindow.h"

class ProjectManager : public QObject
{
    Q_OBJECT
public:
    ProjectManager( QString nectarPath = "", QObject *parent = 0 );
    ~ProjectManager();

    bool newProject();
    void openProject( QString projectPath );
    bool closeProject();
    QStringList getRecentProjects();
    QString getNectarPath() const;

    void setNectarPath( QString path );

    Project *getCurrentProject() const;
    bool isProjectOpen() const;
    bool isProjectOpen( QString path );

signals:
    void recentsProjectsChanged( QStringList );

private:
    void addPathToRecentsProjects( QString projectPath );
private:
    Project *m_currentProject;
    QString m_nectarPath;
    bool m_isOpened;
};

#endif // PROJECTMANAGER_H
