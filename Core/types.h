#ifndef TYPES_H
#define TYPES_H

struct IndividualColorData {
    double position;
    QColor color;
};

#endif // TYPES_H
