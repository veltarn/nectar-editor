#include "conversion.h"


QString Conversion::toHex( QColor color )
{
    return color.name();
}

QColor Conversion::fromHex( QString hex )
{
    QColor c;
    c.setNamedColor( hex );
    return c;
}
