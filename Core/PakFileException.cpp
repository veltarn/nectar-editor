/* 
 * File:   PakFileException.cpp
 * Author: shen
 * 
 * Created on 13 août 2013, 14:01
 */

#include "PakFileException.hpp"

using namespace std;

PakFileException::PakFileException( string text )  throw() : exception(), m_errorMessage( text )
{
}

PakFileException::~PakFileException() throw()
{}

const char *PakFileException::what() const throw()
{
    return m_errorMessage.c_str();
}
