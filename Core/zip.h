#ifndef ZIP_H
#define ZIP_H

#include <iostream>
#include <map>
#include <cmath>
#include <string>
#include <QString>
#include <QFile>

#define BUFFER_SIZE static_cast<quint64>(1024)

typedef unsigned char uint8;

class Zip
{
public:
    void addFile( std::string file, std::string nameInArchive );
    void removeFile( std::string file );
    bool createArchive( std::string archiveName );
private:
    //      source,         destination
    std::map<std::string, std::string> m_filePaths;
};

#endif // ZIP_H
