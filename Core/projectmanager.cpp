#include "projectmanager.h"

ProjectManager::ProjectManager(QString nectarPath, QObject *parent) : QObject( parent ), m_currentProject( NULL ), m_nectarPath( nectarPath ), m_isOpened( false )
{
}

ProjectManager::~ProjectManager()
{
    if( m_currentProject != NULL)
    {
        m_currentProject->close();
        delete m_currentProject;
        m_currentProject = NULL;
    }
}

bool ProjectManager::newProject()
{
    NewProjectWindow npw( 0 );
    if( npw.exec() == QDialog::Accepted )
    {
        m_currentProject = npw.getProject();
        if( m_currentProject != NULL )
        {
            m_isOpened = true;
            addPathToRecentsProjects( m_currentProject->getProjectPath() );
            return true;
        }
    }

    return false;
}

void ProjectManager::openProject( QString projectPath )
{
    if( m_currentProject != NULL )
    {
        m_currentProject->close();
        delete m_currentProject;
        m_currentProject = NULL;
    }

    m_currentProject = new Project( projectPath, 0 );

    if( m_currentProject->load() )
    {
        m_isOpened = true;
    }

    addPathToRecentsProjects( projectPath );
}

QString ProjectManager::getNectarPath() const
{
    return m_nectarPath;
}

bool ProjectManager::isProjectOpen() const
{
    return m_isOpened;
}

bool ProjectManager::isProjectOpen( QString path )
{
    if( m_currentProject != NULL )
    {
        if( m_currentProject->getProjectPath() == path)
            return true;
        else
            return false;
    }

    return false;
}

Project *ProjectManager::getCurrentProject() const
{
    return m_currentProject;
}

void ProjectManager::setNectarPath( QString path )
{
    m_nectarPath = path;
}

void ProjectManager::addPathToRecentsProjects( QString projectPath )
{
    QSettings recentProjects( "data/recent.nec", QSettings::IniFormat );

    QStringList projects = recentProjects.value( "Projects", QStringList() ).toStringList();
    bool foundProject = false;
    if( projects.size() > 0 )
    {
        for( int i = 0; i < projects.size(); ++i )
        {
            QString cProject = projects.at( i );
            if( cProject == projectPath )
                foundProject = true;
        }

        if( !foundProject )
        {
            projects.push_front( projectPath );
            if( projects.size() > 5 )
            {
                projects.pop_back();
            }
            recentProjects.setValue( "Projects", projects );
        }

        emit recentsProjectsChanged( projects );
    }
    else
    {
        projects.push_back( projectPath );
        recentProjects.setValue( "Projects", projects );
        emit recentsProjectsChanged( projects );
    }
}

QStringList ProjectManager::getRecentProjects()
{
    QSettings recentProjects( "data/recent.nec", QSettings::IniFormat );

    return recentProjects.value( "Projects", QStringList() ).toStringList();
}


bool ProjectManager::closeProject()
{
    if( m_currentProject != NULL )
        return m_currentProject->close();
    return true;
}
