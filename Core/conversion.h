#ifndef CONVERSION_H
#define CONVERSION_H

#include <QString>
#include <QColor>

class Conversion
{
public:

    static QString toHex( QColor color );
    static QColor fromHex( QString hex );
};

#endif // CONVERSION_H
