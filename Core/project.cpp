#include "project.h"

using namespace std;

Project::Project(QObject *parent) :
    QObject(parent),
    m_projectName( "" ),
    m_projectAuthor( "" ),
    m_projectPath( "" ),
    m_isValid( false ),
    m_projectJsonFile( this ),
    m_musicPath( "" ),
    m_screenshotPath( "" )
{
    readOptions();
}

Project::Project( QString path, QObject *parent ) :
    QObject( parent ),
    m_projectPath( path ),
    m_isValid( false ),
    m_projectJsonFile( this ),
    m_musicPath( "" ),
    m_screenshotPath( "" )
{
    readOptions();
}

bool Project::load()
{
    m_projectJsonFile.setFileName( m_projectPath );
    m_projectJsonFile.open( QIODevice::ReadOnly );


    QByteArray jsonFileBuffer = m_projectJsonFile.readAll();
    m_jsonFile = QJsonDocument::fromJson( jsonFileBuffer );
    if( m_jsonFile.isNull() )
    {
        std::cerr << "An error has occured during project loading" << std::endl << "Error message: " << m_projectJsonFile.errorString().toStdString() << std::endl;
        QMessageBox::critical( 0, tr( "Error" ), tr( "Cannot read project file ") );
        return false;
    }
    else
        qDebug() << "Successfully read " << m_projectPath << " project" << endl;

    QJsonObject rootObject = m_jsonFile.object();

    QJsonObjectIterator nameIte = rootObject.find( "name" );

    if( nameIte != rootObject.end() )
    {
        m_projectName = nameIte.value().toString();
        qDebug() << "Project name is " << m_projectName << endl;
    }

    QJsonObjectIterator authorIte = rootObject.find( "author" );

    if( authorIte != rootObject.end() )
    {
        m_projectAuthor = authorIte.value().toString();
    }

    m_skycolor.clear();
    QJsonObjectIterator skyGradientIterator = rootObject.find( "skyColor" );
    if( skyGradientIterator != rootObject.end() )
    {
        QJsonArray skyArray = skyGradientIterator.value().toArray();
        for( int i = 0; i < skyArray.size(); ++i )
        {
            QJsonObject cValue = skyArray.at( i ).toObject();
            QJsonObjectIterator posIte = cValue.find( "pos" );
            QJsonObjectIterator valueIte = cValue.find( "value" );

            IndividualColorData colorData;
            colorData.position = posIte.value().toDouble();
            colorData.color = Conversion::fromHex( valueIte.value().toString() );
            m_skycolor.push_back( colorData );
        }
    }
    qDebug() << "Loaded " << m_skycolor.size() << " gradients" << endl;
    //Find maximum players
    QJsonObjectIterator maxPlayersIte = rootObject.find( "maximumPlayers" );
    if( maxPlayersIte != rootObject.end() )
    {
        m_maximumPlayers = static_cast<int>( maxPlayersIte.value().toDouble() );
    }

    //Find world music
    QJsonObjectIterator musicIte = rootObject.find( "music" );
    if( musicIte != rootObject.end() )
    {
        m_musicPath = musicIte.value().toString();
    }

    QJsonObjectIterator screenshotIte = rootObject.find( "screenshot" );
    if( screenshotIte != rootObject.end() )
    {
        m_screenshotPath = screenshotIte.value().toString();
    }

    //Finding world size
    QJsonObjectIterator worldSizeIte = rootObject.find( "worldSize" );
    if( worldSizeIte != rootObject.end() )
    {
        QSize worldSize;
        QJsonObject worldSizeObj = worldSizeIte.value().toObject();
        QJsonObjectIterator widthIte = worldSizeObj.find( "width" );
        QJsonObjectIterator heightIte = worldSizeObj.find( "height" );

        worldSize.setWidth( widthIte.value().toDouble() );
        worldSize.setHeight( heightIte.value().toDouble() );

        m_mapSize = worldSize;
        m_blockNumber.setWidth( ceil( m_mapSize.width() / 16.f ) );
        m_blockNumber.setHeight( ceil( m_mapSize.height() / 16.f ) );

    }

    QJsonObjectIterator backgroundsIte = rootObject.find( "backgrounds" );
    if( backgroundsIte != rootObject.end() )
    {
        QJsonArray backgrounds = backgroundsIte.value().toArray();

        for( int i = 0; i < backgrounds.size(); ++i )
        {
            QString background = backgrounds.at( i ).toString();
            m_backgroundList.append( background );
        }
        qDebug() << "level backgrounds list: " << endl;
        qDebug() << m_backgroundList << endl;
    }

    m_isValid = true;
    return true;
}

bool Project::load( QString path )
{
    m_projectPath = path;
    return load();
}

void Project::setModified( bool modified )
{
    m_isModified = modified;
}

bool Project::isModified() const
{
    return m_isModified;
}


bool Project::isValid() const
{
    return m_isValid;
}

void Project::setProjectName( QString name )
{
    m_projectName = name;
    m_isModified = true;
    m_isValid = true;
}

void Project::setProjectAuthor( QString author )
{
    m_projectAuthor = author;
    m_isModified = true;
    m_isValid = true;
}

void Project::setProjectPath( QString path )
{
    m_projectPath = path;
    m_isModified = true;
    m_isValid = true;
}

QString Project::getProjectName() const
{
    return m_projectName;
}

QString Project::getProjectAuthor() const
{
    return m_projectAuthor;
}

QString Project::getProjectPath() const
{
    return m_projectPath;
}

void Project::readOptions()
{
    QSettings settings( "data/conf.nec", QSettings::IniFormat );

    settings.beginGroup( "Nectar" );
    m_nectarPath = settings.value( "NectarPath", "" ).toString();
    settings.endGroup();
}

void Project::save()
{
    writeJsonLevelFile();
    QFile projectFile( m_projectPath );
    if( !projectFile.open( QIODevice::WriteOnly ) )
    {
        throw std::string( "Cannot write project file" );
    }

    QTextStream stream( &projectFile );
    stream << m_jsonFile.toJson();

    projectFile.flush();
    projectFile.close();
    setModified( false );
}

void Project::build()
{
    QJsonDocument jsonFile;
    QJsonObject rootObject = jsonFile.object();
    //Opening temporary file to store level.json
    Zip zip;
    QTemporaryFile tmpFile;
    if( !tmpFile.open() )
    {
        QMessageBox::critical( 0, tr( "Error" ), tr( "Cannot correctly save the project..." ) );
        return;
    }

    QTextStream stream( &tmpFile );

    QString name = tmpFile.fileName();

    rootObject.insert( "name", m_projectName );

    rootObject.insert( "author", m_projectAuthor );

    QJsonObject worldSizeObj;
    worldSizeObj.insert( "width", m_mapData->size() * 16.f );
    worldSizeObj.insert( "height",m_mapData->at( 0 ).size() * 16.f );

    rootObject.insert( "worldSize", worldSizeObj );


    QJsonArray skyGradArray = writeSkyGradient();
    if( !skyGradArray.isEmpty() )
        rootObject.insert( "skyColor", skyGradArray );

    QJsonValue v( m_maximumPlayers );
    rootObject.insert( "maximumPlayers", v );

    if( m_screenshotPath != "" )
    {
        QFileInfo screenshotPathInfo( m_screenshotPath );
        rootObject.insert( "screenshot", screenshotPathInfo.fileName() );
    }

    QJsonArray mapArray = writeMap();
    rootObject.insert( "mapDefinition", mapArray );

    if( !m_musicPath.isEmpty() && m_musicPath != "" )
    {
        QFileInfo musicPathInfo( m_musicPath );
        QString musicPathInBinFile( "music/" + musicPathInfo.fileName() );

        rootObject.insert( "music", musicPathInBinFile );
        zip.addFile( m_musicPath.toStdString(), musicPathInBinFile.toStdString() );
    }


    if( backgroundsNumber() > 0 )
    {
        QJsonArray backgroundsArray;
        for( QStringList::iterator it = m_backgroundList.begin(); it != m_backgroundList.end(); ++it )
        {
            QFileInfo fi( *it );
            QString filenameInBinFile( "backgrounds/" + fi.fileName() );
            QJsonValue v( filenameInBinFile );
            backgroundsArray.append( v );
            qDebug() << "Saving " << *it << " to binary file as " << filenameInBinFile << endl;
            zip.addFile( it->toStdString(), QString( "backgrounds/" + fi.fileName() ).toStdString() );
        }
        rootObject.insert( "backgrounds", backgroundsArray );
    }

    if( m_screenshotPath != "" )
    {
        QFileInfo screenshotPathInfo( m_screenshotPath );
        zip.addFile( m_screenshotPath.toStdString(), screenshotPathInfo.fileName().toStdString() );
    }

    jsonFile.setObject( rootObject );
    stream << jsonFile.toJson();

    stream.flush();

    zip.addFile( name.toStdString(), "level.json" );

    if( m_nectarPath.isEmpty() )
    {
        while( m_nectarPath.isEmpty() )
        {
            QMessageBox::warning( 0, tr( "Warning" ), tr( "You must specify a path to the game before build the level" ) );

            m_nectarPath = QFileDialog::getExistingDirectory( 0, tr( "Specify Nectar directory" ) );
        }
    }

    QDir dir( m_nectarPath );
    if( dir.cd( "data/maps" ) )
    {
        QFileInfo projectPathInfo( m_projectPath );
        QString builtLevelPath( dir.path() + "/" + projectPathInfo.completeBaseName() + ".nel" );
        qDebug() << builtLevelPath << endl;

        //If a previous built file exists, we remove it before build the new
        QFile oldFile( builtLevelPath );
        QFileInfo oldFileInfo( oldFile );
        if( oldFileInfo.exists() )
            oldFile.remove();

        //level.create( builtLevelPath.toStdString() );
        if( !zip.createArchive( builtLevelPath.toStdString() ) ) {
            QMessageBox::critical( 0, tr( "Error" ), tr( "An error has occured during the compression of the level" ) );
        }
    }
    else
    {
        qDebug() << "Cannot find data/maps directory into " << m_nectarPath << endl;
        qDebug() << "REMPLACER CA PAR UNE PU*AIN D'EXCEPTION" << endl;
    }

    tmpFile.close();
    setModified( false );
}

void Project::writeJsonLevelFile( bool building )
{
    QJsonObject rootObject = m_jsonFile.object();

    rootObject.insert( "name", m_projectName );

    rootObject.insert( "author", m_projectAuthor );

    QJsonObject worldSizeObj;
    worldSizeObj.insert( "width", m_mapData->size() * 16.f );
    worldSizeObj.insert( "height",m_mapData->at( 0 ).size() * 16.f );

    rootObject.insert( "worldSize", worldSizeObj );

    QJsonValue v( m_maximumPlayers );
    rootObject.insert( "maximumPlayers", v );    

    if( m_screenshotPath != "" )
    {
        rootObject.insert( "screenshot", m_screenshotPath);
    } else {
        rootObject.remove( "screenshot" );
    }

    if( !m_musicPath.isEmpty() && m_musicPath != "" )
    {
        rootObject.insert( "music", m_musicPath );
    }

    QJsonArray skyGradArray = writeSkyGradient( );
    if( !skyGradArray.isEmpty() )
        rootObject.insert( "skyColor", skyGradArray );

    QJsonArray mapArray = writeMap( );
    rootObject.insert( "mapDefinition", mapArray );

    if( backgroundsNumber() > 0 )
    {
        QJsonArray backgroundsArray;
        for( QStringList::iterator it = m_backgroundList.begin(); it != m_backgroundList.end(); ++it )
        {
            QFileInfo fi( *it );
            QString filename;
            if( building )
                filename =  "backgrounds/" + fi.fileName();
            else
                filename = *it;

            QJsonValue v( filename );
            backgroundsArray.append( v );
        }
        rootObject.insert( "backgrounds", backgroundsArray );
    }
    else
    {
        rootObject.remove( "backgrounds" );
    }
    m_jsonFile.setObject( rootObject );
}

QJsonArray Project::writeSkyGradient( )
{
    QJsonArray rootArray;
    for( int i = 0; i < m_skycolor.size(); ++i )
    {
        IndividualColorData colorData = m_skycolor.at( i );
        QString color = Conversion::toHex( colorData.color );
        double position = colorData.position;

        QJsonObject cObject;
        cObject.insert( "pos", position );
        cObject.insert( "value", color );

        rootArray.append( cObject );
    }
    return rootArray;
}

QJsonArray Project::writeMap( )
{
    QJsonArray rootArray;
    for( int y = 0; y < m_mapData->at( 0 ).size(); ++y )
    {
        QJsonArray row;
        for( int x = 0; x < m_mapData->size(); ++x )
        {
            MapCase *mCase = (*m_mapData)[x][y];
            int materialType = 0;
            if( mCase != NULL )
            {
                materialType = mCase->caseType;
            }

            row.append( materialType );
        }
        rootArray.append( row );
    }

    return rootArray;
}


bool Project::close()
{
    if( m_isModified )
    {
        int rep = QMessageBox::question( 0, tr( "Confirmation" ), QString( tr( "This project is about to close but was not saved.") + "<br />" + tr( "Do you want to save the project ?" ) ), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel );

        if( rep == QMessageBox::Yes )
        {
            save();
        }
        else if( rep == QMessageBox::Cancel )
        {
            return false;
        }

        return true;
    }

    return true;
}


QSize Project::getMapSize() const
{
    return m_mapSize;
}

QSize Project::getBlocksNumber() const
{
    return m_blockNumber;
}

void Project::setBlocksNumber( int x, int y )
{
    setBlocksNumber( QSize( x, y ) );
}

void Project::setBlocksNumber( QSize num )
{
    m_blockNumber = num;
    m_mapSize.setWidth( 0 );
    m_mapSize.setHeight( 0 );

    m_mapSize.setWidth( ceil( m_blockNumber.width() * 16.f ) );
    m_mapSize.setHeight( ceil( m_blockNumber.height() * 16.f ) );
}

unsigned int Project::getMaximumPlayers() const
{
    return m_maximumPlayers;
}

void Project::setMaximumPlayers( unsigned int p )
{
    m_maximumPlayers = p;
}

void Project::setMapDataPointer( MultiGraphicRectItemVector *mapData )
{
    m_mapData = mapData;
}

void Project::syncMapData( GraphicScene *scene )
{
    MaterialSelector mSelector;
    if( m_projectJsonFile.isOpen() )
    {
        if( !m_jsonFile.isNull() )
        {
            QJsonObject rootObject = m_jsonFile.object();

            const QJsonObjectIterator mapDefinition = rootObject.find( "mapDefinition" );


            if( mapDefinition != rootObject.end() ) //If something has been found
            {
                QJsonArray mapArray = mapDefinition.value().toArray();
                for( unsigned int y = 0; y < mapArray.size(); ++y )
                {
                    QJsonArray row = mapArray.at( y ).toArray();

                    for( unsigned int x = 0; x < row.size(); ++x )
                    {
                        int number = static_cast<int>( row[x].toDouble() );
                        MapCase *mCase = new MapCase;
                        mCase->caseType = number;

                        if( number > 0 )
                        {
                            BasicMaterial *material = mSelector.getMaterial( number );
                            if( material != NULL )
                            {
                                QGraphicsRectItem *item = new QGraphicsRectItem( QRectF( x * 16.f, y * 16.f, 16, 16 ) );
                                item->setBrush( material->getBrush() );
                                scene->addItem( item );
                                mCase->item = item;

                                (*m_mapData)[x][y] = mCase;
                            }
                            else
                            {
                                qDebug() << "[ERROR] Unable to determine material type [" << number << "]" << endl;
                            }
                        }


                    }
                }
            }
            else
            {
                qDebug() << "[ERROR] Cannot find mapDefinition object" << endl;
            }
        }
    }
    else
    {
        std::cerr << "Cannot read json level file" << std::endl;
    }

    m_projectJsonFile.close();
}

void Project::addBackground( QString background )
{
    if( !backgroundExists( background ) )
    {
        m_backgroundList.push_back( background );
    }
}

void Project::setBackgroundList( QStringList &backgroundList )
{
    m_backgroundList = backgroundList;
}

void Project::removeBackground( QString background )
{
    if( backgroundExists( background ) )
    {
        QStringList::iterator it = findBackground( background );
        if( it != m_backgroundList.end() )
        {
            m_backgroundList.erase( it );
        }
    }
}

void Project::clearBackgroundList()
{
    m_backgroundList.clear();
}

QStringList::iterator Project::findBackground( QString background )
{
    for( QStringList::iterator it = m_backgroundList.begin(); it != m_backgroundList.end(); ++it )
    {
        if( *it == background )
            return it;
    }

    return m_backgroundList.end();
}

bool Project::backgroundExists( QString background )
{
    for( QStringList::iterator it = m_backgroundList.begin(); it != m_backgroundList.end(); ++it )
    {
        if( *it == background )
            return true;
    }

    return false;
}

unsigned int Project::backgroundsNumber()
{
    return m_backgroundList.size();
}

QStringList *Project::backgrounds()
{
    return &m_backgroundList;
}

void Project::setMusicFilePath( QString path )
{
    m_musicPath = path;
}

QString Project::getMusicFilePath() const
{
    return m_musicPath;
}

void Project::setScreenshotPath( QString path )
{
    m_screenshotPath = path;
}

QString Project::getScreenshotPath() const
{
    return m_screenshotPath;
}

void Project::setSkyGradient( QVector<IndividualColorData> sky )
{
    m_skycolor = sky;
}

QVector<IndividualColorData> Project::getSkyGradient() const
{
    return m_skycolor;
}
