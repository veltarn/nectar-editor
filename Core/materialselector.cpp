#include "materialselector.h"

MaterialSelector::MaterialSelector() : m_currentMaterial( NULL )
{
    m_materialList["Void"] = 0;
    m_materialList["Ground"] = 1;
    m_materialList["Stone"] = 2;
    m_materialList["Sand"] = 3;
    m_materialList["Wood"] = 4;
    m_materialList["Water"] = 5;
}

QMap<QString, int> MaterialSelector::getMaterialList() const
{
    return m_materialList;
}

BasicMaterial *MaterialSelector::getMaterial( QString name )
{
    BasicMaterial *material = NULL;
    if( name == "Ground" )
    {
        material = new GroundMaterial;
    } else if( name == "Stone" ) {
        material = new StoneMaterial;
    } else if( name == "Sand" ) {
        material = new SandMaterial;
    } else if( name == "Wood" ) {
        material = new WoodMaterial;
    } else if( name == "Water" ) {
        material = new WaterMaterial;
    }

    return material;

}

BasicMaterial *MaterialSelector::getMaterial( int materialId )
{
    for( QMap<QString, int>::iterator mapIter = m_materialList.begin(); mapIter != m_materialList.end(); ++mapIter )
    {
        if( mapIter.value() == materialId )
        {
            return getMaterial( mapIter.key() );
        }
    }

    return NULL;
}
