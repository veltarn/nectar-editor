/* 
 * File:   PakFileException.hpp
 * Author: shen
 *
 * Created on 13 août 2013, 14:01
 */

#ifndef PAKFILEEXCEPTION_HPP
#define	PAKFILEEXCEPTION_HPP

#include <iostream>
#include <exception>

class PakFileException : public std::exception
{
public:
    PakFileException( std::string text ) throw();    
    virtual const char *what() const throw();
    
    virtual ~PakFileException() throw();
private:
    std::string m_errorMessage;
};

#endif	/* PAKFILEEXCEPTION_HPP */

