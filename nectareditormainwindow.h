#ifndef NECTAREDITORMAINWINDOW_H
#define NECTAREDITORMAINWINDOW_H

#define CONFIGURATION_FILE "data/conf.nec"

#include <QDebug>
#include <QMainWindow>
#include <QMap>
#include <QToolBar>
#include <QAction>
#include <QWidget>
#include <QLabel>
#include <QDockWidget>
#include <QStatusBar>
#include <QProgressDialog>
#include <QProgressBar>
#include <QVector>
#include <QFileInfo>
#include <QMenuBar>
#include <QMenu>
#include <QSize>
#include <QDir>
#include <QComboBox>
#include <QSettings>
#include <QGridLayout>
#include <QCloseEvent>
#include <QStandardPaths>
#include "Gui/firststartdialog.h"
#include "Gui/optionswindow.h"
#include "Core/projectmanager.h"
#include "Gui/projectpropertieswindow.h"
#include "Gui/newprojectwindow.h"
#include "Graphics/graphicalview.h"
#include "Graphics/brushtool.h"
#include "Graphics/erasertool.h"
#include "Core/materialselector.h"

namespace Ui {
class NectarEditorMainWindow;
}

class NectarEditorMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit NectarEditorMainWindow(QWidget *parent = 0);
    ~NectarEditorMainWindow();

    void buildInterface();
    void createToolbar();
    void createDockWidget();
    void createStatusBar();
    void initEvents();
    void initProjectEvents();
    void removeProjectEvents();
    void openProject( QString path );
    void enableTools();
    void disableTools();
public slots:
    void openOptionWindow();
    void onOpenProjectClicked();
    void onRecentProjectsChanged( QStringList projects );
    void recentProjectClicked();
    void openProjectProperties();
    void createNewProject();
    void onBrushSelected();
    void onEraserSelected();
    void onMaterialChoosed( QString name );
    void onZoomFactorChanged( float z );
    void onRectangleSelectionEnabled( bool enabled );
    void onMouseMoved( QPoint win, QPointF scene );
    void onSave();
    void onBuild();
protected:
    void closeEvent(QCloseEvent *);

private:
    void createWorkspace();
    void readOptions();
    void saveOptions();
    void freeRecentsProjectsActions();
    void createGraphicContext();
    void destroyGraphicContext();
private:
    Ui::NectarEditorMainWindow *ui;
    QSettings m_configurationFile;

    QWidget *m_centralWidget;

    //Actions
    QAction *m_newLevelAction;
    QAction *m_openLevelAction;
    QAction *m_saveAction;
    QAction *m_saveAsAction;
    QAction *m_buildAction;
    QAction *m_quitAction;

    QAction *m_undoAction;
    QAction *m_redoAction;

    QAction *m_projectPropertiesAction;

    QAction *m_optionsAction;

    QAction *m_aboutQtAction;
    QAction *m_aboutAction;

    QAction *m_pointerAction;
    QAction *m_brushAction;
    QAction *m_eraserAction;
    QAction *m_fillAction;
    QAction *m_colorPickerAction;

    QLabel *m_zoomFactorLabel;
    QLabel *m_windowCoordsLabel;
    QLabel *m_sceneCoordsLabel;

    QVector<QAction *> m_recentProjects;

    QMenu *m_mainMenu;
    QMenu *m_editMenu;
    QMenu *m_projectMenu;
    QMenu *m_toolsMenu;
    QMenu *m_helpMenu;

    QComboBox *m_textureChooser;


    QToolBar *m_dockTools;

    ProjectManager *m_projectManager;

    QString m_softwareName;
    QString m_workspacePath;

    GraphicalView *m_graphicContext;

    MaterialSelector m_materialSelector;

};

#endif // NECTAREDITORMAINWINDOW_H
