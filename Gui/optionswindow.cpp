#include "optionswindow.h"
#include "ui_optionswindow.h"

OptionsWindow::OptionsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsWindow),
    m_configurationFile( "data/conf.nec", QSettings::IniFormat )
{
    ui->setupUi(this);
    setWindowTitle( "Options" );
    setFixedSize( 500, 300 );
    readOptions();

    initEvents();
}

OptionsWindow::~OptionsWindow()
{
    delete ui;
}

void OptionsWindow::readOptions()
{
    QString nectarPath = m_configurationFile.value( "Nectar/NectarPath", "" ).toString();
    ui->nectarPathLineEdit->setText( nectarPath );
}

void OptionsWindow::initEvents()
{
    connect( ui->nectarPathBrowseButton, SIGNAL( clicked() ), this, SLOT( onBrowseButtonClicked() ) );
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( onValidateBtnClicked() ) );
}

void OptionsWindow::saveOptions()
{
    QString path = ui->nectarPathLineEdit->text();
    m_configurationFile.beginGroup( "Nectar" );
    m_configurationFile.setValue( "NectarPath", path );
    m_configurationFile.endGroup();
}

void OptionsWindow::onValidateBtnClicked()
{
    saveOptions();
    accept();
}

void OptionsWindow::onBrowseButtonClicked()
{
    QString path = QFileDialog::getExistingDirectory( this, tr( "Select Nectar folder" ), ui->nectarPathLineEdit->text() );

    if( !path.isEmpty() )
        ui->nectarPathLineEdit->setText( path );
    else
        ui->nectarPathLineEdit->setText( "" );
}
