#ifndef PROJECTPROPERTIESWINDOW_H
#define PROJECTPROPERTIESWINDOW_H

#include <QDialog>
#include <QDebug>
#include <QUrl>
#include <QtMultimedia>
#include <QMessageBox>
#include <QFileDialog>
#include <QColorDialog>
#include <QFileInfo>
#include <QStringList>
#include <QListWidgetItem>
#include <QSettings>
#include <QStandardPaths>
#include <QPalette>
#include "../Core/conversion.h"
#include "../Core/types.h"
#include "../Widgets/listwidget.h"
#include "../Widgets/gradientwidget.h"
#include "../Core/projectmanager.h"

//Gérer le drag n drop
namespace Ui {
class ProjectPropertiesWindow;
}

class ProjectPropertiesWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit ProjectPropertiesWindow(ProjectManager *projectManager, QWidget *parent = 0);
    ~ProjectPropertiesWindow();

    void fillFields();
public slots:
    void onValidate();
    void addBackground();
    void removeBackground();
    void onOrderChanged( QStringList newOrder );
    void onSelectList( QModelIndex index );
    void onBrowseMusic();
    void onPlayPressed();
    void onStopPressed();
    void onPausePressed();
    void onCancel();
    void onBrowseScreenshotPressed();
    void onScreenshotPreviewPressed();
    void onAddColorPressed();
    void onRemoveColorPressed();
    void onColorPositionChanged( double v );
    void onColorPickerClicked();
    void selectColor( int index );
private:
    void initEvents();
    void enableMusicControlButtons();
    void disableMusicControlButtons();
    bool allowedExtension( QString extension );
private:
    Ui::ProjectPropertiesWindow *ui;
    GradientWidget *m_gradientWidget;
    ListWidget *m_listWidget;
    ProjectManager *m_projectManager;
    bool m_selectedItem;
    QMediaPlayer m_mediaPlayer;
};

#endif // PROJECTPROPERTIESWINDOW_H
