#ifndef FIRSTSTARTDIALOG_H
#define FIRSTSTARTDIALOG_H

#include <QDialog>
#include <QString>
#include <QStandardPaths>
#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>

#define BASIC_PATH QString( QStandardPaths::writableLocation( QStandardPaths::DocumentsLocation ) + "/NectarEditor/projects/" )

namespace Ui {
class FirstStartDialog;
}

class FirstStartDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FirstStartDialog( QWidget *parent = 0, QString *workspacePath = NULL );
    ~FirstStartDialog();
public slots:
    void onBrowse();
    void onValidate();
protected:
    //void closeEvent( QCloseEvent *e );
private:
    void initEvents();
private:
    Ui::FirstStartDialog *ui;

    QString *m_workspacePath;
};

#endif // FIRSTSTARTDIALOG_H
