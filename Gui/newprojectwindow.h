#ifndef NEWPROJECTWINDOW_H
#define NEWPROJECTWINDOW_H

#include <cmath>
#include <QDebug>
#include <QString>
#include <QDialog>
#include <QSpinBox>
#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QDir>
#include "../Core/project.h"

namespace Ui {
class NewProjectWindow;
}

class NewProjectWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit NewProjectWindow(QWidget *parent = 0);
    ~NewProjectWindow();
    
    Project *getProject();
public slots:
    //From world size to cell numbers
    void calculateCellsNumbersX();
    void calculateCellsNumbersY();

    //From cell numbers to world size
    void calculateWorldSizeX();
    void calculateWorldSizeY();

    void onValidate();
    void onBrowseButtonClicked();
    void onLevelNameChanged( QString name );
private:
    void initEvents();
    void readOptions();
    bool verifyFields();
    void createProjectDirectory();
private:
    Ui::NewProjectWindow *ui;
    QString m_workspacePath;
    Project *m_project;
};

#endif // NEWPROJECTWINDOW_H
