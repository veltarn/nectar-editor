#include "projectpropertieswindow.h"
#include "ui_projectpropertieswindow.h"

ProjectPropertiesWindow::ProjectPropertiesWindow(ProjectManager *projectManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProjectPropertiesWindow),
    m_listWidget( NULL ),
    m_projectManager( projectManager ),
    m_selectedItem( false )
{
    ui->setupUi(this);
    QString musicPath;
    QFileInfo projectPathInfo( m_projectManager->getCurrentProject()->getProjectPath() );
    musicPath = projectPathInfo.path() + "/music";
    ui->musicLocationLabel->setText( musicPath );

    m_gradientWidget = new GradientWidget( this );
    ui->gradientWidgetLayout->addWidget( m_gradientWidget );

    m_listWidget = new ListWidget( this );
    ui->listLayout->addWidget( m_listWidget );
    ui->playButton->setIcon( QIcon( QPixmap( "data/resources/play.png" ) ) );
    ui->pauseButton->setIcon( QIcon( QPixmap( "data/resources/pause.png" ) ) );
    ui->stopButton->setIcon( QIcon( QPixmap( "data/resources/stop.png" ) ) );

    ui->addColorButton->setIcon( QIcon( QPixmap( "data/resources/add.png" ) ) );
    ui->removeColorButton->setIcon( QIcon( QPixmap( "data/resources/remove.png" ) ) );
    ui->colorPicker->setIcon( QIcon( QPixmap( "data/resources/color_picker.png" ) ) );

    ui->tabWidget->setTabIcon( 0, QIcon( QPixmap( "data/resources/general.png" ) ) );
    ui->tabWidget->setTabIcon( 1, QIcon( QPixmap( "data/resources/world.png" ) ) );
    ui->tabWidget->setTabIcon( 2, QIcon( QPixmap( "data/resources/background.png" ) ) );
    ui->tabWidget->setTabIcon( 3, QIcon( QPixmap( "data/resources/gradient.png" ) ) );
    ui->tabWidget->setTabIcon( 4, QIcon( QPixmap( "data/resources/music.png" ) ) );

    disableMusicControlButtons();
    setWindowTitle( tr( "Project properties" ) );

    fillFields();
    initEvents();
}

ProjectPropertiesWindow::~ProjectPropertiesWindow()
{
    if( m_listWidget != NULL )
    {
        delete m_listWidget;
        m_listWidget = NULL;
    }

    delete ui;
}


void ProjectPropertiesWindow::fillFields()
{
    Project *currentProject = m_projectManager->getCurrentProject();
    ui->levelNameLineEdit->setText( currentProject->getProjectName() );
    ui->authorLineEdit->setText( currentProject->getProjectAuthor() );
    ui->xCaseSpinBox->setValue( currentProject->getBlocksNumber().width() );
    ui->yCaseSpinBox->setValue( currentProject->getBlocksNumber().height() );

    ui->lineEditMusicPath->setText( currentProject->getMusicFilePath() );
    ui->screenshotLineEdit->setText( currentProject->getScreenshotPath() );
    if( currentProject->getMusicFilePath() != "" )
    {
        enableMusicControlButtons();
    }

    //Filling background colors
    QVector<IndividualColorData> skyGradient = currentProject->getSkyGradient();
    if( skyGradient.size() > 0 )
    {
        for( int i = 0; i < skyGradient.size(); ++i )
        {
            IndividualColorData cData = skyGradient.at( i );

            m_gradientWidget->addColor( cData.position, cData.color );
            ui->colorSelector->setMaximum( m_gradientWidget->colorNumber() - 1 );
            selectColor( 0 );
        }
    }

    QStringList *backgroundsList = currentProject->backgrounds();
    if( backgroundsList->size() > 0 )
    {
        for( QStringList::iterator it = backgroundsList->begin(); it != backgroundsList->end(); ++it )
        {
            m_listWidget->addItem( *it );
        }
    }
}

void ProjectPropertiesWindow::initEvents()
{
    connect( ui->cancelButton, SIGNAL( clicked() ), this, SLOT( onCancel() ) );
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
    connect( ui->backgroundAddButton, SIGNAL( clicked() ), this, SLOT( addBackground() ) );
    connect( ui->backgroundRemoveButton, SIGNAL( clicked() ), this, SLOT( removeBackground() ) );
    connect( m_listWidget, SIGNAL( clicked( QModelIndex ) ), this, SLOT( onSelectList( QModelIndex ) ) );
    connect( m_listWidget, SIGNAL( orderChanged( QStringList ) ), this, SLOT( onOrderChanged( QStringList ) ) );
    connect( ui->browseMusicButton, SIGNAL( clicked() ), this, SLOT( onBrowseMusic() ) );
    connect( ui->playButton, SIGNAL( clicked() ), this, SLOT( onPlayPressed() ) );
    connect( ui->pauseButton, SIGNAL( clicked() ), this, SLOT( onPausePressed() ) );
    connect( ui->stopButton, SIGNAL( clicked() ), this, SLOT( onStopPressed() ) );
    connect( ui->browseScreenshotButton, SIGNAL( clicked() ), this, SLOT( onBrowseScreenshotPressed() ) );
    connect( ui->screenshotPreviewButton, SIGNAL( clicked() ), this, SLOT( onScreenshotPreviewPressed() ) );
    connect( ui->addColorButton, SIGNAL( clicked() ), this, SLOT( onAddColorPressed() ) );
    connect( ui->removeColorButton, SIGNAL( clicked() ), this, SLOT( onRemoveColorPressed() ) );
    connect( ui->colorSelector, SIGNAL( valueChanged(int) ), this, SLOT( selectColor(int) ) );
    connect( ui->colorPosition, SIGNAL( valueChanged(double) ), this, SLOT( onColorPositionChanged(double) ) );
    connect( ui->colorPicker, SIGNAL( clicked() ), this, SLOT( onColorPickerClicked() ) );
}

void ProjectPropertiesWindow::onCancel()
{
    QString path =  m_projectManager->getCurrentProject()->getMusicFilePath();

    if( !path.isEmpty() )
    {
        QFile file( path );
        if( file.remove() )
        {
            qDebug() << "Deleted temporary import file" << endl;
        }
    }

    accept();
}

void ProjectPropertiesWindow::onValidate()
{
    Project *currentProject = m_projectManager->getCurrentProject();
    currentProject->setProjectName( ui->levelNameLineEdit->text() );
    currentProject->setProjectAuthor( ui->authorLineEdit->text() );
    currentProject->setBlocksNumber( ui->xCaseSpinBox->value(), ui->yCaseSpinBox->value() );

    /*for( int i = 0; i < m_listWidget->count(); ++i )
    {
        QListWidgetItem *item = m_listWidget->item( i );
        currentProject->addBackground( item->text() );
        qDebug() << item->text() << endl;
    }*/
    currentProject->setSkyGradient( m_gradientWidget->getColorGradient() );
    qDebug() << *currentProject->backgrounds() << endl;
    qDebug() << currentProject->backgroundsNumber() << " backgrounds added" << endl;

    qDebug() << currentProject->getMusicFilePath() << " added to music resources" << endl;

    accept();
}

void ProjectPropertiesWindow::addBackground()
{
    QString filename = QFileDialog::getOpenFileName( 0, tr( "Add a background" ), QString(), "Images (*.png *.jpg *.jpeg *.bmp)" );
    Project *project = m_projectManager->getCurrentProject();
    assert( project != NULL );

    if( !filename.isEmpty() && !project->backgroundExists( filename ) )
    {
        //Copying file to $NectarProject$/backgrounds
        QFileInfo filenameInfo( filename );
        QFileInfo projectPathInfo( m_projectManager->getCurrentProject()->getProjectPath() );
        QString projectPath( projectPathInfo.path() );
        QString backgroundPath( projectPath + "/backgrounds/" + filenameInfo.fileName() );

        QFile origFile( filename );
        if( !origFile.copy( backgroundPath ) )
        {
            QMessageBox::critical( this, tr( "Error" ), tr( "Cannot import background into the project" ) );
            return;
        }

        m_listWidget->addItem( backgroundPath );
        project->addBackground( backgroundPath );
    }
}

void ProjectPropertiesWindow::removeBackground()
{
    QListWidgetItem *item = m_listWidget->currentItem();

    if( m_selectedItem )
    {
        QString data = item->text();
        qDebug() << data << endl;
        QFile bckFile( data );
        bool ok = bckFile.remove();
        qDebug() << ok << endl;
        //m_listWidget->removeItemWidget( item );
        delete item;
        m_projectManager->getCurrentProject()->removeBackground( data );

        if( m_listWidget->count() == 0 )
            m_selectedItem = false;
    }

    qDebug() << *m_projectManager->getCurrentProject()->backgrounds() << endl;
}

void ProjectPropertiesWindow::onSelectList( QModelIndex index )
{
    QString data = index.data().toString();
    if( !data.isEmpty() )
    {
        ui->backgroundRemoveButton->setEnabled( true );
        m_selectedItem = true;
    }
}

void ProjectPropertiesWindow::onOrderChanged( QStringList newOrder )
{
    m_projectManager->getCurrentProject()->setBackgroundList( newOrder );
}

void ProjectPropertiesWindow::onBrowseMusic()
{
    QString musicPath = QStandardPaths::writableLocation( QStandardPaths::MusicLocation );
    QString filter = tr( "Music " ) + "(*.ogg *.wav *.flac *.mp3)";
    QString file = QFileDialog::getOpenFileName( this, tr( "Choose a music file" ), musicPath, filter );

    if( !file.isEmpty() )
    {
        QFileInfo fileInfo( file );

        if( fileInfo.exists() )
        {
            if( allowedExtension( fileInfo.suffix() ) )
            {
                //Deleting current music file if exists
                QString musicFilePath = m_projectManager->getCurrentProject()->getMusicFilePath();
                if( musicFilePath != "" )
                {
                    qDebug() << "deleting existing music resource" << endl;
                    QFile file( musicFilePath );
                    bool ok = file.remove();
                    qDebug() << ok << endl;
                }

                QFileInfo selectedFileInfo( file );
                QFileInfo projectPathInfo( m_projectManager->getCurrentProject()->getProjectPath() );
                QString destPath( projectPathInfo.path() + "/music/" + selectedFileInfo.fileName() );

                QFile orig( file );
                if( !orig.copy( destPath ) )
                {
                    QMessageBox::critical( this, tr( "Error" ), tr( "Cannot import music file into the project" ) );
                    return;
                }

                ui->lineEditMusicPath->setText( destPath );
                m_projectManager->getCurrentProject()->setMusicFilePath( destPath );
                enableMusicControlButtons();
            }
        }
    }
}

void ProjectPropertiesWindow::enableMusicControlButtons()
{
    ui->playButton->setEnabled( true );
    ui->pauseButton->setEnabled( true );
    ui->stopButton->setEnabled( true );
}

void ProjectPropertiesWindow::disableMusicControlButtons()
{
    ui->playButton->setEnabled( false );
    ui->pauseButton->setEnabled( false );
    ui->stopButton->setEnabled( false );
}

bool ProjectPropertiesWindow::allowedExtension( QString extension )
{
    QStringList allowedExtensions;
    allowedExtensions << "flac" << "ogg" << "wav" << "mp3";

    for( int i = 0; i < allowedExtensions.size(); ++i )
    {
        if( extension == allowedExtensions.at( i ) )
            return true;
    }
    return false;
}

void ProjectPropertiesWindow::onPlayPressed()
{
    QUrl path = QUrl::fromLocalFile( ui->lineEditMusicPath->text() );
    m_mediaPlayer.setMedia( path );
    m_mediaPlayer.play();
}

void ProjectPropertiesWindow::onStopPressed()
{
    if( m_mediaPlayer.state() == QMediaPlayer::PlayingState )
    {
        m_mediaPlayer.stop();
    }
}

void ProjectPropertiesWindow::onPausePressed()
{
    if( m_mediaPlayer.state() == QMediaPlayer::PlayingState )
    {
        m_mediaPlayer.pause();
    }
}

void ProjectPropertiesWindow::onBrowseScreenshotPressed()
{
    QString path = QFileDialog::getOpenFileName( this, tr( "Choose a picture to describe your level" ), QString(), "Images (*.png *.jpeg *.jpg *.bmp)" );

    if( path != "" )
    {
        //Deleting screenshot if already exists
        if( m_projectManager->getCurrentProject()->getScreenshotPath() != "" )
        {
            QFile file( m_projectManager->getCurrentProject()->getScreenshotPath() );
            file.remove();
            qDebug() << "Deleted existing screenshot file" << endl;
        }

        //Copying screenshot to project folder
        QFileInfo origPathInfo( path );
        QFileInfo destPathInfo( m_projectManager->getCurrentProject()->getProjectPath() );
        QString screenshotPath( destPathInfo.path() + "/screenshot." + origPathInfo.completeSuffix() );

        QFile file( path );
        if( !file.copy( screenshotPath ) )
        {
            qDebug() << "Cannot copy" << path << " to " << destPathInfo.path() << endl;
        }

        m_projectManager->getCurrentProject()->setScreenshotPath( screenshotPath );
        ui->screenshotLineEdit->setText( screenshotPath );
    }
}

void ProjectPropertiesWindow::onScreenshotPreviewPressed()
{
    QLabel *label = new QLabel( 0 );
    label->setWindowTitle( tr( "Screenshot preview" ) );
    QPixmap screenshotPrev( m_projectManager->getCurrentProject()->getScreenshotPath() );

    label->setPixmap( screenshotPrev );
    label->show();
}

void ProjectPropertiesWindow::onAddColorPressed()
{
    unsigned int index = m_gradientWidget->addColor( 0.5, QString( "#aaaaaa" ) );
    ui->colorSelector->setMaximum( m_gradientWidget->colorNumber() - 1 );
    selectColor( index );
}

void ProjectPropertiesWindow::onRemoveColorPressed()
{
    int index = ui->colorSelector->value();
    m_gradientWidget->removeColor( index );
    ui->colorSelector->setMaximum( m_gradientWidget->colorNumber() - 1 );
    selectColor( index - 1 );
}

void ProjectPropertiesWindow::onColorPositionChanged( double v )
{
    int index = ui->colorSelector->value();
    m_gradientWidget->setPositionOf( index, v );
}

void ProjectPropertiesWindow::onColorPickerClicked()
{
    QColor color( Qt::white );
    int index = ui->colorSelector->value();
    color = Conversion::fromHex( m_gradientWidget->colorAt( index ) );
    color = QColorDialog::getColor( color, this );
    if( color.isValid() )
    {
        m_gradientWidget->setColorAt( index, color );
        //Updating values
        selectColor( index );
    }
}

void ProjectPropertiesWindow::selectColor( int index )
{
    double position = m_gradientWidget->getPositionOf( index );
    QString color = m_gradientWidget->colorAt( index );
    QSpinBox *selector = qobject_cast<QSpinBox*>( sender() );
    //If the user didn't choosed the color from the spin box
    if( selector == NULL )
    {
        ui->colorSelector->disconnect( SIGNAL( valueChanged(int) ) );
        ui->colorSelector->setValue( index );
        connect( ui->colorSelector, SIGNAL( valueChanged(int) ), this, SLOT( selectColor(int) ) );
    }
    QPalette palette;
    palette.setColor( QPalette::Text, Conversion::fromHex( color ) );
    ui->colorPosition->setValue( position );
    ui->color->setPalette( palette );
    ui->color->setText( color );
}
