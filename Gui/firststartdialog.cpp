#include "firststartdialog.h"
#include "ui_firststartdialog.h"

FirstStartDialog::FirstStartDialog( QWidget *parent, QString *workspacePath ) :
    QDialog(parent),
    ui(new Ui::FirstStartDialog),
    m_workspacePath( workspacePath )
{
    ui->setupUi(this);
    setWindowTitle( tr( "Setting workspace directory" ) );
    ui->recommendedPathLabel->setText( BASIC_PATH );
    ui->lineEditPath->setText( BASIC_PATH );

    *m_workspacePath = BASIC_PATH;

    initEvents();
}

FirstStartDialog::~FirstStartDialog()
{
    delete ui;
}

void FirstStartDialog::initEvents()
{
    connect( ui->browseButton, SIGNAL( clicked() ), this, SLOT( onBrowse() ) );
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
}

void FirstStartDialog::onBrowse()
{
    QString path = QFileDialog::getExistingDirectory( this, tr( "Workspace directory" ) );

    if( !path.isEmpty() )
    {
        *m_workspacePath = path;
        ui->lineEditPath->setText( *m_workspacePath );
    }
}

void FirstStartDialog::onValidate()
{
    if( m_workspacePath->isEmpty() )
    {
        QMessageBox::warning( this, tr( "Warning" ), tr( "You must specify a workspace directory" ) );
    }
    else
    {
        accept();
    }
}
