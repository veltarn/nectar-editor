#include "newprojectwindow.h"
#include "ui_newprojectwindow.h"

NewProjectWindow::NewProjectWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewProjectWindow),
    m_project( NULL )
{
    ui->setupUi(this);
    readOptions();
    initEvents();
    ui->worldSizeGroupBox->hide();
    setWindowTitle( "New Level" );
}

NewProjectWindow::~NewProjectWindow()
{
    delete ui;
}

void NewProjectWindow::initEvents()
{
    /*connect( ui->worldSizeX, SIGNAL( editingFinished() ), this, SLOT( calculateCellsNumbersX() ) );
    connect( ui->worldSizeY, SIGNAL( editingFinished() ), this, SLOT( calculateCellsNumbersY() ) );
    connect( ui->cellsNumberX, SIGNAL( editingFinished() ), this, SLOT( calculateWorldSizeX() ) );
    connect( ui->cellsNumberY, SIGNAL( editingFinished() ), this, SLOT( calculateWorldSizeY() ) );*/
    connect( ui->createButton, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
    connect( ui->browseButton, SIGNAL( clicked() ), this, SLOT( onBrowseButtonClicked() ) );
    connect( ui->cancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( ui->levelNameLineEdit, SIGNAL( textChanged( QString) ), this, SLOT( onLevelNameChanged( QString ) ) );
}

void NewProjectWindow::readOptions()
{
    QSettings settings( "data/conf.nec", QSettings::IniFormat );

    m_workspacePath = settings.value( "NectarEditor/WorkspacePath", "" ).toString();

    QString filename = ui->levelNameLineEdit->text().replace( " ", "" ) + ".nep";
    ui->levelPathLineEdit->setText( m_workspacePath + ui->levelNameLineEdit->text() + "/" + filename );
}

void NewProjectWindow::onBrowseButtonClicked()
{
    QString levelName = ui->levelNameLineEdit->text();
    if( levelName.isEmpty() )
        levelName = "Untitled Map";
    QString path = QFileDialog::getSaveFileName( this, tr( "Select file name of your level" ), m_workspacePath + "/" + levelName + "/untitled.nep" );

    if( !path.isEmpty() )
    {
        ui->levelPathLineEdit->setText( path );
    }
}

void NewProjectWindow::calculateCellsNumbersX()
{
    qDebug() << "CalculateCellsNumberX" << endl;
    QSpinBox *spin = qobject_cast<QSpinBox*>( sender() );
    int cell = spin->value() * 16.f ;

    ui->cellsNumberX->setValue( cell );
}

void NewProjectWindow::calculateCellsNumbersY()
{
    qDebug() << "CalculateCellsNumberY" << endl;
    QSpinBox *spin = qobject_cast<QSpinBox*>( sender() );
    int cell = spin->value() * 16.f ;

    ui->cellsNumberY->setValue( cell );
}

void NewProjectWindow::calculateWorldSizeX()
{
    qDebug() << "calculateWorldSizeX" << endl;
    QSpinBox *spin = qobject_cast<QSpinBox*>( sender() );
    int size = spin->value() * 16.f;
    ui->worldSizeX->setValue( size );
}

void NewProjectWindow::calculateWorldSizeY()
{
    qDebug() << "calculateWorldSizeY" << endl;
    QSpinBox *spin = qobject_cast<QSpinBox*>( sender() );
    int size = spin->value() * 16.f;
    ui->worldSizeY->setValue( size );
}

void NewProjectWindow::onValidate()
{
    if( verifyFields() )
    {
        createProjectDirectory();
        m_project = new Project;
        m_project->setProjectName( ui->levelNameLineEdit->text() );
        m_project->setProjectAuthor( ui->authorLineEdit->text() );
        m_project->setProjectPath( ui->levelPathLineEdit->text() );
        m_project->setBlocksNumber( ui->cellsNumberX->value(), ui->cellsNumberY->value() );
        m_project->setMaximumPlayers( ui->maxPlayersNumber->value() );

        accept();
    }
}


bool NewProjectWindow::verifyFields()
{
    bool error = false;
    QStringList errorList;

    if( ui->levelNameLineEdit->text().isEmpty() )
    {
        error = true;
        errorList.push_back( tr( "Level name is missing" ) );
    }

    if( ui->authorLineEdit->text().isEmpty() )
    {
        error = true;
        errorList.push_back( tr( "Author name is missing" ) );
    }

    if( ui->levelPathLineEdit->text().isEmpty() )
    {
        error = true;
        errorList.push_back( tr( "Level save path is missing" ) );
    }

    if( error )
    {
        QString msg = "The following fields are missing: <br /><ul>";

        for( int i = 0; i < errorList.size(); ++i )
        {
            msg += "<li>" + errorList.at( i ) + "</li>";
        }

        msg += "</ul>";

        QMessageBox::warning( this, tr( "Some fields are missing" ), msg );
    }

    //Return false if error == true and true if error == false
    return !error;
}

void NewProjectWindow::createProjectDirectory()
{
    QDir workspaceDir( m_workspacePath );
    QString projectDir( ui->levelNameLineEdit->text() );
    if( workspaceDir.mkdir( projectDir ) )
    {
        workspaceDir.cd( projectDir );
        //Creating basic directories structure
        workspaceDir.mkdir( "backgrounds" );
        workspaceDir.mkdir( "music" );
    }
}

Project *NewProjectWindow::getProject()
{
    return m_project;
}

void NewProjectWindow::onLevelNameChanged( QString name )
{
    QString folderName = name;
    QString filename = name.replace( " ", "" ) + ".nep";
    QString fullName = m_workspacePath + folderName + "/" + filename;

    ui->levelPathLineEdit->setText( fullName );
}
