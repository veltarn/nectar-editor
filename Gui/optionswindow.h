#ifndef OPTIONSWINDOW_H
#define OPTIONSWINDOW_H

#include <QDialog>
#include <QPushButton>
#include <QTableWidget>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSettings>
#include <QFileDialog>

namespace Ui {
class OptionsWindow;
}

class OptionsWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit OptionsWindow(QWidget *parent = 0);
    ~OptionsWindow();
    
    void saveOptions();
public slots:
    void onValidateBtnClicked();
    void onBrowseButtonClicked();
private:
    void readOptions();
    void initEvents();

private:
    Ui::OptionsWindow *ui;

    QSettings m_configurationFile;

};

#endif // OPTIONSWINDOW_H
