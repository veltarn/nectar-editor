#ifndef GRADIENTWIDGET_H
#define GRADIENTWIDGET_H

#include <iostream>
#include <QDebug>
#include <QLabel>
#include <QRect>
#include <QPainter>
#include <QPaintEvent>
#include <QLinearGradient>
#include <QVector>
#include <QString>
#include <QVariant>
#include "../Core/conversion.h"
#include "../Core/types.h"

/**
 * @brief This class is used to provide a easy to use widget that previews a gradient
 */
class GradientWidget : public QLabel
{
    Q_OBJECT

public:
    explicit GradientWidget(QWidget *parent = 0);

    unsigned int addColor( double position, QColor color );
    unsigned int addColor( double position, QString color );

    void setColorAt( int index, QString color );
    void setColorAt( int index, QColor color );
    QString colorAt( int index );

    void setPositionOf( int index, double position );
    double getPositionOf( int index ) const;

    void removeColor( int index );

    unsigned int colorNumber() const;

    QVector<IndividualColorData> getColorGradient();
signals:

public slots:

protected:
    virtual void paintEvent( QPaintEvent *pe );

private:
    QVector<IndividualColorData> m_colors;

};

#endif // GRADIENTWIDGET_H
