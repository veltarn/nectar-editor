#include "gradientwidget.h"

GradientWidget::GradientWidget(QWidget *parent) :
    QLabel(parent)
{
}

unsigned int GradientWidget::addColor( double position, QColor color )
{
    IndividualColorData nColor;
    nColor.position = position;
    nColor.color = color;

    m_colors.push_back( nColor );
    repaint();
    return m_colors.size() - 1;
}

unsigned int GradientWidget::addColor( double position, QString color )
{
    qDebug() << "Adding color (" << color <<") QColor val: " << Conversion::fromHex( color ) << endl;
    return addColor( position, Conversion::fromHex( color ) );
}

void GradientWidget::setColorAt( int index, QString color )
{
    setColorAt( index, Conversion::fromHex( color ) );
}

void GradientWidget::setColorAt( int index, QColor color )
{
    if( index < m_colors.size() )
    {
        IndividualColorData colorData = m_colors.at( index );
        colorData.color = color;
        m_colors[index] = colorData;
        repaint();
    }
}

QString GradientWidget::colorAt( int index )
{
    if( index < m_colors.size() )
    {
        IndividualColorData c = m_colors.at( index );
        QColor color = c.color;
        return Conversion::toHex( color );
    }
    else
    {
        return "#000";
    }
}

void GradientWidget::setPositionOf( int index, double position )
{
    if( index < m_colors.size() )
    {
        IndividualColorData colorData = m_colors.at( index );
        colorData.position = position;
        m_colors[index] = colorData;
        repaint();
    }
}

double GradientWidget::getPositionOf( int index ) const
{
    if( index < m_colors.size() )
        return m_colors.at( index ).position;
    else
        return 0.0;
}

void GradientWidget::removeColor( int index )
{
    if( index < m_colors.size() )
    {
        m_colors.erase( m_colors.begin() + index );
        repaint();
    }
}

void GradientWidget::paintEvent( QPaintEvent *pe )
{
    Q_UNUSED( pe );
    QPainter painter( this );
    QRect labelRect = this->geometry();
    if( m_colors.size() > 0 )
    {
        QLinearGradient gradient( 0, 0, 0, this->height() );
        gradient.setSpread( QGradient::PadSpread );

        //Looping into m_colors to create the gradient
        for( QVector<IndividualColorData>::iterator it = m_colors.begin(); it != m_colors.end(); ++it )
        {
            qDebug() << it->position << " ==> " << it->color << endl;
            gradient.setColorAt( it->position, it->color );
        }

        painter.setBrush( QBrush( gradient ) );
        painter.drawRect( 0, 0, labelRect.width(), labelRect.height() );
    }
    else
    {
        QColor c( 255, 255, 255 );
        painter.setBrush( QBrush( c ) );
        painter.drawRect( 0, 0, labelRect.width(), labelRect.height() );
    }
}

unsigned int GradientWidget::colorNumber() const
{
    return m_colors.size();
}

QVector<IndividualColorData> GradientWidget::getColorGradient()
{
    return m_colors;
}
