#include "listwidget.h"

ListWidget::ListWidget(QWidget *parent) :
    QListWidget(parent)
{
    this->setDragEnabled( true );

    this->setDragDropMode( QAbstractItemView::InternalMove );
}

void ListWidget::dropEvent( QDropEvent *event )
{
    QListWidget::dropEvent( event );

    QStringList list;

    for( int i = 0; i < this->count(); ++i )
    {
        QListWidgetItem *item = this->item( i );
        list.push_back( item->text() );
    }

    emit orderChanged( list );
}
