#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include <QDebug>
#include <QStringList>
#include <QListWidget>

class ListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit ListWidget(QWidget *parent = 0);
    
signals:
    void orderChanged( QStringList list );
public slots:

protected:
    virtual void dropEvent( QDropEvent *event );
    
};

#endif // LISTWIDGET_H
