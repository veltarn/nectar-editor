#include <iostream>
#include "nectareditormainwindow.h"
#include <QApplication>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    NectarEditorMainWindow w;
    w.show();
    int ret = a.exec();


    return ret;
}
